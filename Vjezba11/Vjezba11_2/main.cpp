#include <iostream>
#include <algorithm>
#include <time.h>
using namespace std;

//PUNI VEKTOR S 10 NASUMICNIH INTEGERA
void FillVectorRand(vector<int> &vec)
{
    for(int i = 0 ; i < 10 ; i++)
    {
        vec.push_back(rand() % 17 + 1);
    }
}

//PROVJERAVA DA LI JE BROJ NEPARAN
bool Odd(int x)
{
    return x % 2;
}

//TRAZI VECE OD 100
bool FindGreaterThan100(int x)
{
    return x>100;
}

//PROVJERAVA DA LI JE POTENCIJA BROJA DVA
bool FindPow2(int x)
{
    if(x != 0 && (x&(x-1)) == 0)
        return true;
    return false;
}

//VRACA ITERATOR NA MINIMALNI CLAN VEKTORA
vector<int>::iterator FindMin(vector<int> &vec)
{
    vector<int>::iterator it = min_element(vec.begin(),vec.end());
    return it;
}

//VRACA ITERATOR NA PRVI NEPARNI U VEKTORU AKO POSTOJI
vector<int>::iterator FindOdd(vector<int> &vec)
{
    vector<int>::iterator it = find_if(vec.begin(), vec.end(), Odd);
    return it;
}

//VRACA INT COUNTER U KOJEM JE ZAPISANO KOLIKO JE NEPARNIH U VEKTORU
int CountOdd(vector<int> &vec)
{
    int CO = count_if(vec.begin(), vec.end(), Odd);
    return CO;
}

//PREKO REMOVE_IF SORTIRA A PREKO COUNT_IF RACUNA KOLIKO IH JE PREKO 100 I ONDA RESIZE-a VEKTOR DA OSTANU SAMO MANJI OD 100
void RemoveGreaterThan100(vector<int> &vec)
{
    int CountGreater100 = count_if(vec.begin(), vec.end(), FindGreaterThan100);
    vector<int>::iterator it = remove_if(vec.begin(), vec.end(), FindGreaterThan100);
    vec.resize(vec.size() - CountGreater100);
}

//AKO JE POTENCIJA BROJA 2 ONDA MIJENJA TAJ BROJ S 2
void ChangeIfPow2(vector<int> &vec)
{
    replace_if(vec.begin(), vec.end(), FindPow2, 2);
}

//SORTIRANJE VEKTORA
bool SortVector( const int &left, const int &right)
{
    if(left % 2 && right % 2)
        return left < right;
    else if(left % 2)
        return false;
    else if(right % 2)
        return true;
    return left < right;
}

int main()
{
    srand(time(nullptr));
    vector<int> vec;
    FillVectorRand(vec);

    for(vector<int>::iterator i = vec.begin() ; i != vec.end() ; i++)
        cout<<*i<<endl;
    cout<<endl<<endl;

    /*//TRAZI MINIMALNI CLAN
    vector<int>::iterator it = FindMin(vec);
    if(vec.end() == it)
        cout<<"Nema minimalnog, svi su jednaki."<<endl;
    else
        cout<<*it<<endl;
    */

    /*//TRAZI PRVI NEPARNI CLAN
    vector<int>::iterator it = FindOdd(vec);
    if(vec.end() == it)
        cout<<"Nema neparnog"<<endl;
    else
        cout<<*it<<endl;
    */

    /*//BROJI NEPARNE BROJEVE
    int OddCounter = CountOdd(vec);
    if(OddCounter == 1)
        cout<<"U vektoru je 1 neparni broj."<<endl;
    else if (OddCounter == 2 || OddCounter == 3 || OddCounter == 4)
        cout<<"U vektoru su "<<OddCounter<<" neparna broja."<<endl;
    else
        cout<<"U vektoru je "<<OddCounter<<" neparnih brojeva."<<endl;
    */
    /* //BRISE VECE OD 100
    RemoveGreaterThan100(vec);
    for(vector<int>::iterator i = vec.begin() ; i != vec.end() ; i++)
    {
        cout<<*i<<endl;
    }
    */
    /*//TRAZI POTENCIJU BROJA DVA I MINJA JE S DUJOM
    ChangeIfPow2(vec);
    for(vector<int>::iterator i = vec.begin() ; i != vec.end() ; i++)
    {
        cout<<*i<<endl;
    }
    */
    /*//SORTIRA PARNE PA NEPARNE BROJEVE UNUTAR VEKTORA
    sort(vec.begin(), vec.end(), SortVector);
    for(vector<int>::iterator i = vec.begin() ; i != vec.end() ; i++)
    {
        cout<<*i<<endl;
    }
    */






}
