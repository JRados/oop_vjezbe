// C++ program to demonstrate the use of std::min_element
#include <iostream>
#include <algorithm>
using namespace std;

// Defining the BinaryFunction
bool comp(int a, int b)
{
	return (a > b);
}

int main1()
{
	int v[] = { 9, 4, 7, 2, 5, 1, 11, 12, 3, 3, 6 };

	// Finding the minimum value between the third and the
	// ninth element
	// Znaci da funkcija provjerava svaki element izme�u i na temelju
	// vlastite funkcije vraca rezultat

	int* i1;
	i1 = std::min_element(v + 2, v + 9, comp);

	cout << *i1 << "\n";
	return 0;
}

