#include <iostream>
#include<fstream>
#include<string>
#include<ctime>

using namespace std;

class NotInt : public exception
{
public:
    virtual const char* what() const throw()
    {
        return "Not an int!";
    }
};

class NotOperand : public exception
{
public:
    virtual const char* what() const throw()
    {
        return "Not an operand!";
    }
};

class Zero : public exception
{
public:
    virtual const char* what() const throw()
    {
        return "Can't divide with zero";
    }
};

class ErrorLog
{
public:
    static void Log(string tekst)
    {
        ofstream file_obj ("ERROR.txt", ios::out | ios::app);
        file_obj<<tekst<<endl;
        /*time_t now = time(0);
        char str[26];
        ctime_s(str, sizeof str, &now);
        cout<<str;*/
        file_obj.close();
    }
};

int CheckNumber()
{
    int x;
    cout<<"Unesi broj:\t";
    if(!(cin>>x))
        throw NotInt();
    return x;

}

char CheckOperator()
{
    char c;
    cout<<"Unesi operator:\t";
    cin>>c;
    if(c != '+' && c != '-' && c != '*' && c != '/')
        throw NotOperand();
    return c;
}

int CheckResult(int x, int y, char c)
{
    if(c == '/')
    {
        if (y == 0)
            throw Zero();
        return x/y;
    }
    if(c == '*')
        return x*y;
    if(c == '+')
        return x+y;
    if(c == '-')
        return x-y;
}

int main()
{
    int x,y, rez, flag;
    char o;
    do{
        try{
            x = CheckNumber();
            y = CheckNumber();
            o = CheckOperator();
            rez = CheckResult(x,y,o);
            cout<<"Rezultat je: "<<rez<<endl<<"Da li cete prekinuti unos?"<<endl
            <<"1-Ne 0-DA"<<endl;
            cin>>flag;

        }
        catch(exception& ex){
            string str = ex.what();
            ErrorLog::Log(ex.what());
            break;
        }
    }while(flag != 0);
}
