#ifndef ZBROJ_H
#define ZBROJ_H
#include<stdlib.h>
template <typename T>

class Zbroj
{
private:
    T _a;
    T _b;
public:
    Zbroji(T a, T b){_a = a; _b = b;}
    T GetA(){return _a;}
    T GetB(){return _b;}
    void SetA(T a){_a = a;}
    void SetB(T b){_b = b;}
    T Sum(){return _a + _b;}
};

template <>

class Zbroj<char>
{
private:
    char _a;
    char _b;
public:
    Zbroji(char a, char b){_a = a; _b = b;}
    char GetA() const{return _a;}
    char GetB()const{return _b;}
    void SetA(char a){_a = a;}
    void SetB(char b){_b = b;}
    char Sum()
    {
        if(isdigit(_a) && isdigit(_b))
        {
            return ((_a - '0') + (_b - '0') + '0');
        }
        else
        {
            return (_a - '0') + (_b - '0');
        }
    }


};
#endif // ZBROJ_H

