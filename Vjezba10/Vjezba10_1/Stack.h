#ifndef STACK_H
#define STACK_H

#include <iostream>
#include <vector>
using namespace std;

template <typename T>

class Stack
{
    private:
        const int _MaxSize = 1000;
        T _top;
        vector<T> _vec;
        T* _StackPtr;
        int _size;

    public:
        Stack(int s = 10)
        {
            _size = s;
            _StackPtr = nullptr;
            _vec.reserve(10);
        }

        ~Stack() {}

        int GetSize()
        {
            _size = _vec.size();
            return _vec.size();
        }

        T GetTop()
        {
            if(!CheckStack()){
                _top = _vec.back();
                return _vec.back();
            }
        }

        bool CheckStack()
        {
            if(_vec.size() == 0)
            {
                cout<<"Stack is empty."<<endl;
                return true;
            }
            if(_vec.size() == _MaxSize)
            {
                cout<<"Stack is full !!!"<<endl;
                return false;
            }
            return true;
        }

        T PopBack()
        {
            if(!CheckStack() || (CheckStack() && GetSize()!=0))
            {
                T a = _vec.back();
                _vec.pop_back();
                _StackPtr = &_vec.back();
                return a;
            }
            else
                _StackPtr = nullptr;
        }

        void PushBack(T a)
        {
            if(CheckStack())
            {
                _vec.push_back(a);
                _StackPtr = &_vec.back();
            }
            else
                cout<<"Stack overflow!!!"<<endl;
        }

};

#endif // STACK_H
