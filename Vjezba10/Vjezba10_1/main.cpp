#include <iostream>
#include "stack.h"
using namespace std;

int main()
{
    Stack<int> s;
    for(int i=100;i<1100;i++){ s.PushBack(i); }

    cout<<s.GetSize()<<endl;
    int a=s.PopBack();
    cout<<a<<endl;
    cout<<s.GetSize()<<endl;
    cout<<"TEST"<<s.GetTop()<<endl;

}
