
#include<iostream>
#include<vector>
#include<new>
#include<iomanip>
#include<algorithm>

using namespace std;


vector<int> unesi_vrijednosti2(int a=0, int b=100, int n=5, bool var = true)
{
    vector<int> v;
    if(var)
    {
        for(int i=0; i<n; i++)
        {
            v.push_back(rand()%b + a);
        }
    }
    else
    {
        for(int i=0; i<n ; i++)
        {
            int x;
            cout<<"Upisite vrijednost elementa vektora:  ";
            do{
                cin>>x;
            }while(x > 100 || x < 0);
            v.push_back(x);

        }

    }
    return v;
}

void ispisi_vektor2(vector<int> v)
{
    for(int i=0;i<v.size();i++)
    {
        cout<<v[i]<<endl;
    }
}

int main()
{
    vector <int> vec;
    srand (time(NULL));
    vec =  unesi_vrijednosti2(0,100,5,false);
    ispisi_vektor2(vec);
}

