#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include "Food.h"

using namespace std;

int Rand()
{
    int x = rand() % 1000 +10;
}

int main()
{
    vector<Food> Restoraunt ;
    Restoraunt.assign(5,Food());
    srand(static_cast <unsigned> (time(nullptr)));

    //PUNJENJE POLJA
    Restoraunt[0].SetFood("Pita","Pita od sira", 102.5, 12.0, 24.4, 35.2, "13-05-2021", 10.2);
    Restoraunt[1].SetFood("Jelo","Manistra", 205.3, 22.0, 44.4, 20.2, "13-05-2021", 40.5);
    Restoraunt[2].SetFood("Pita","Pita od mesa", 122.5, 32.0, 24.4, 31.2, "13-05-2021", 12.2);
    Restoraunt[3].SetFood("Jelo","Odrezak", 202.5, 18.0, 24.4, 40.2, "13-05-2021", 30.2);
    Restoraunt[4].SetFood("Prilog","Pire", 302.5, 16.0, 34.4, 41.2, "13-05-2021", 31.2);

    //NIZ I PROMJENA DNEVNE POTROSNJE U SLUCAJU PORASTA ILI PADA PRODAJE
    for(int i = 0; i<5; i++)
    {
        Restoraunt[i].SetArrayMonthly();
        Restoraunt[i].ChangeDailyAmount(2020);

    }

    //ISPIS OBJEKTA I DESTRUKTOR
    for(int i = 0 ; i<5 ; i++)
    {
        Restoraunt[i].PrintClass();
        Restoraunt[i].~Food();
    }

}
