#ifndef POTROSNJA_H
#define POTROSNJA_H
#include<string>

using namespace std;

class MonthlyAmount
{

    private:

        int year;
        int month;
        double amount;

    public:

        const int GetYear();
        const int GetMonth();
        const double GetAmount();
        void InnerSet(double k, int y, int m );
};

#endif // POTROSNJA_H







#ifndef FOOD_H
#define FOOD_H
#include <string>

using namespace std;

class Food{

    private:

        string _type;
        string _name;
        double _water;
        double _protein;
        double _fat;
        double _carbs;
        string _endDate;
        double _AmountPerDay;
        MonthlyAmount *_monthly;
        int _ArraySize;

    public:

        Food(string type = "0", string name = "0", double water = 0.0, double protein = 0.0, double fat = 0.0, double carbs = 0.0, string date = "01-01-1900", double AmountPerDay = 0.0);

        Food(const Food &p2);

        ~Food();

        int GetCurrentYear();
        int GetCurrentMonth();
        int GetEndYear();

        double SetRandValue(int low, int high);

        void SetArrayMonthly();

        const MonthlyAmount GetMonthly(int m);

        void ChangeAmountPerDay(bool flag);

        const double GetAmountPerDay();

        void SetMonthly(double amount, int y, int m);

        void SetFood(string type, string name, double water, double protein, double fat, double carbs, string date, double AmountPerDay);

        double DetectPercent(int year);

        void ChangeDailyAmount(int year);

        const void PrintClass();

        void Test2();

};
#endif // FOOD_H

