#ifndef WITCHER3_H
#define WITCHER3_H
#include<string>
#include<vector>
#include"OpenWorld.h"
#include"RPG.h"
class Witcher3 : public OpenWorld//, public RPG
{
private:
    vector<string> _platforms;
    string _name;
    string _type;
public:
    Witcher3();//{_type = "OpenWorld and RPG";}
    vector<string> GetPlatforms();
    string GetName();
    void SetPlatforms(string str);
    void Test(string line);


};
#endif // WITCHER3_H
