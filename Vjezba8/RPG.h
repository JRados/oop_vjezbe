#ifndef RPG_H
#define RPG_H
#include "VideoGames.h"
class RPG : public VideoGames
{

protected:
    string _type = "RPG";
public:

    RPG();
    string GetType();
    virtual vector<string> GetPlatforms();


};
#endif // RPG_H
