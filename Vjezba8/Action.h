#ifndef ACTION_H
#define ACTION_H
#include "VideoGames.h"
class Action : public VideoGames
{
protected:
    const string _type = "Action";
public:

    Action();
    string GetType();
    virtual vector<string> GetPlatforms();


};
#endif // ACTION_H
