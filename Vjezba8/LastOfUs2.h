#ifndef LASTOFUS2_H
#define LASTOFUS2_H
#include "Action.h"
class LastOfUs2 : public Action
{
private:
    vector<string> _platforms;
    string _name;
public:
    LastOfUs2();
    vector<string> GetPlatforms();
    string GetName();
    void SetPlatforms(string str);
    void Test(string line);
};
#endif // LASTOFUS2_H
