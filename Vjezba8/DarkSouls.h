#ifndef DARKSOULS_H
#define DARKSOULS_H
#include<vector>
#include<string>
#include "RPG.h"
class DarkSouls : public RPG
{
private:
    vector<string> _platforms;
    string _name;
public:
    DarkSouls();
    vector<string> GetPlatforms();
    string GetName();
    void SetPlatforms(string str);
    void Test(string line);

};
#endif // DARKSOULS_H
