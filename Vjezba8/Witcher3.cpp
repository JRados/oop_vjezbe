#include "Witcher3.h"

Witcher3::Witcher3(){}

vector<string> Witcher3::GetPlatforms()
{
    return _platforms;
}
string Witcher3::GetName()
{
    return _name;
}

void Witcher3::SetPlatforms(string str)
{
    _platforms.push_back(str);
}

void Witcher3::Test(string line)
{
    if (line.find("PC") != std::string::npos)
            SetPlatforms("PC");
    if (line.find("XBOX") != std::string::npos)
            SetPlatforms("XBOX");
    if (line.find("PS4") != std::string::npos)
            SetPlatforms("PS4");
}

