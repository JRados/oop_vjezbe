#ifndef FALLOUT4_H
#define FALLOUT4_H
#include<vector>
#include<string>
#include "RPG.h"

class Fallout4 : public RPG
{
private:
    vector<string> _platforms;
    string _name;
public:
    Fallout4();
    vector<string> GetPlatforms();
    string GetName();
    void SetPlatforms(string str);
    void Test(string line);

};
#endif // FALLOUT4_H
