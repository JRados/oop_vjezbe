#include "LastOfUs2.h"

LastOfUs2::LastOfUs2(){};

vector<string> LastOfUs2::GetPlatforms()
{
    return _platforms;
}
string LastOfUs2::GetName()
{
    return _name;
}

void LastOfUs2::SetPlatforms(string str)
{
    _platforms.push_back(str);
}

void LastOfUs2::Test(string line)
{
    if (line.find("PC") != std::string::npos)
            SetPlatforms("PC");
    if (line.find("XBOX") != std::string::npos)
            SetPlatforms("XBOX");
    if (line.find("PS4") != std::string::npos)
            SetPlatforms("PS4");
}
