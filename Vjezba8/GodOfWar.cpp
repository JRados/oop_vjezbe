#include "GodOfWar.h"

GodOfWar::GodOfWar(){}
vector<string> GodOfWar::GetPlatforms(){return _platforms;}
void GodOfWar::SetPlatforms(string str)
{
    _platforms.push_back(str);
}

string GodOfWar::GetName()
{
    return _name;
}

void GodOfWar::Test(string line)
{
    if (line.find("PC") != std::string::npos)
            SetPlatforms("PC");
    if (line.find("XBOX") != std::string::npos)
            SetPlatforms("XBOX");
    if (line.find("PS4") != std::string::npos)
            SetPlatforms("PS4");
}
