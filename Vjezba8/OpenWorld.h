#ifndef OPENWORLD_H
#define OPENWORLD_H
#include<iostream>
#include "VideoGames.h"
class OpenWorld : public VideoGames
{

protected:
    string _type = "Open World";
public:

    OpenWorld();
    string GetType();
    virtual vector<string> GetPlatforms();

};
#endif // OPENWORLD_H
