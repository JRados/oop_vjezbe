#include"Fallout4.h"

Fallout4::Fallout4(){};

vector<string> Fallout4::GetPlatforms()
{
    return _platforms;
}
string Fallout4::GetName()
{
    return _name;
}

void Fallout4::SetPlatforms(string str)
{
    _platforms.push_back(str);
}

void Fallout4::Test(string line)
{
    if (line.find("PC") != std::string::npos)
            SetPlatforms("PC");
    if (line.find("XBOX") != std::string::npos)
            SetPlatforms("XBOX");
    if (line.find("PS4") != std::string::npos)
            SetPlatforms("PS4");
}

