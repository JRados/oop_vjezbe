#include "DarkSouls.h"

DarkSouls::DarkSouls(){};

vector<string> DarkSouls::GetPlatforms()
{
    return _platforms;
}
string DarkSouls::GetName()
{
    return _name;
}

void DarkSouls::SetPlatforms(string str)
{
    _platforms.push_back(str);
}

void DarkSouls::Test(string line)
{
    if (line.find("PC") != std::string::npos)
            SetPlatforms("PC");
    if (line.find("XBOX") != std::string::npos)
            SetPlatforms("XBOX");
    if (line.find("PS4") != std::string::npos)
            SetPlatforms("PS4");
}
