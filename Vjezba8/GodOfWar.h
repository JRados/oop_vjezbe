#ifndef GODOFWAR_H
#define GODOFWAR_H
#include "Action.h"
class GodOfWar : public Action
{
private:
    vector<string> _platforms;
    string _name;
public:
    GodOfWar();
    vector<string> GetPlatforms();
    string GetName();
    void SetPlatforms(string str);
    void Test(string line);
};
#endif // GODOFWAR_H
