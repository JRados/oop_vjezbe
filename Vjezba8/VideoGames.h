#ifndef VIDEOGAMES_H
#define VIDEOGAMES_H
#include <string>
#include <vector>

using namespace std;
class VideoGames{

public:
    virtual string GetType() = 0;
    virtual vector<string> GetPlatforms() = 0;
    virtual void Test(string line) = 0;

};
#endif // VIDEOGAMES_H
