#include <iostream>
#include "Vektor.h"

using namespace std;
using namespace oop;

int main()
{

    Vec3 v1(3.0,5.0,8.0);
    Vec3 v2(2.0,5.0,7.0);
    Vec3 v3;

    // PRINTANJE VEKTORA, DI MO�EMO VIDJETI I SAMI KONSTRUKTOR ZA V1 I V2
    // TE DEFAULT VRIJEDNOSTI KONSTRUKTORA ZA V3
    cout<<v1<<v2<<v3<<endl<<endl;

    //PRIMJER UNOSA KOORDINATA U VEKTOR
    //cin>>v3;
    //cout<<v3<<endl<<endl;

    //PRIMJER OPERATORA =
    v3 = v1;
    cout<<v3<<endl<<endl;

    //OPERATORI ZBRAJANJA I ODUZIMANJA VEKTORA
    v3 = v1 + v2;
    cout<<v3;
    v3 = v1 - v2;
    cout<<v3<<endl<<endl;

    //OPERATORI MNJO�ENJA I DIJELJENJA VEKTORA S SKALAROM
    v3 = v1 * 3;
    cout<<v3;
    v3 = v2 / 2;
    cout<<v3<<endl<<endl;

    //OPERATORI +=  -=  *=  /=
    v3+=v3;
    cout<<v3;
    v3-=v1;
    cout<<v3;
    v3*=2;
    cout<<v3;
    v3/=2;
    cout<<v3<<endl<<endl;

    //SKALARNI PRODUKT VEKTORA
    v3 = v1 * v2;
    cout<<v3<<endl<<endl;

    //OPERATOR []
    cout<<v3[1]<<endl;
    cout<<v3[2]<<endl;
    cout<<v3[3]<<endl;
    cout<<v3[4]<<endl<<endl;

    //NORMALIZACIJA VEKTORA
    v3.Clan();

    //COUNTER INSTANCIRANJA
    cout<<endl<<"Counter: "<<v3.GetCounter()<<endl<<endl;

}
