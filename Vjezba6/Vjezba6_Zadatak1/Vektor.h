#ifndef VEC3_H
#define VEC3_H
using namespace std;

namespace oop{

    class Vec3{

    private:

        double _x;
        double _y;
        double _z;
        static int _counter;

    public:

        Vec3(double x = 0.0 , double y = 0.0 , double z = 0.0);
        //~Vec3();

        double Getx() const;
        double Gety() const;
        double Getz() const;
        int GetCounter() const;

        void Setx(double a);
        void Sety(double a);
        void Setz(double a);

        Vec3 & operator = (Vec3 const &obj);

        Vec3 operator + (Vec3 const &obj);

        Vec3 operator - (Vec3 const &obj);

        Vec3  operator * (double const obj);

        Vec3  operator / (double const obj);

        Vec3 & operator += (Vec3 const &obj);

        Vec3 & operator -= (Vec3 const &obj);

        Vec3 & operator *= (double const obj);

        Vec3 & operator /= (double const obj);

        bool  operator == (Vec3 const &obj);

        bool  operator != (Vec3 const &obj);

        Vec3  operator * (Vec3 const &obj);

        double operator [] (int const obj);

        Vec3 Clan();


        friend ostream & operator << (ostream &out, Vec3 &obj)
            {
                out <<"Coordinates of Vector:"<<endl<< "X: "<< obj.Getx()<< " Y: " << obj.Gety()<< " Z: " << obj.Getz()<<endl;
                return out;
            }


        friend istream & operator >> (istream &in, Vec3 &obj)
            {
                cout<<"Enter coordinates e.g.(1.0 2.0 3.0): ";
                in>>obj._x>>obj._y>>obj._z;
                cout<<endl;
                return in;
            }


};
}



#endif // VEC3_H
