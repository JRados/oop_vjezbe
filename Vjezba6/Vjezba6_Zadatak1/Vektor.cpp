#include <iostream>
#include <math.h>
#include "Vektor.h"
using namespace std;
using namespace oop;

int Vec3::_counter = 0;

Vec3::Vec3(double x, double y, double z)
{
    _x = x;
    _y = y;
    _z = z;
    Vec3::_counter +=1;
}

//Vec3::~Vec3()


double Vec3::Getx() const
{
    return _x;
}
double Vec3::Gety() const
{
    return _y;
}
double Vec3::Getz() const
{
    return _z;
}
int Vec3::GetCounter() const
{
    return _counter;
}

void Vec3::Setx(double a)
{
    _x = a;
}

void Vec3::Sety(double a)
{
    _y = a;
}

void Vec3::Setz(double a)
{
    _z = a;
}


Vec3 & Vec3::operator = (Vec3 const &obj) {

    _x = obj._x;
    _y = obj._y;
    _z = obj._z;
    return *this;
}

Vec3 Vec3::operator - (Vec3 const &obj) {
    Vec3 v1;
    v1._x = _x - obj._x;
    v1._y = _y - obj._y;
    v1._z = _z - obj._z;
    return v1;
    }

Vec3 Vec3::operator + (Vec3 const &obj) {

    Vec3 v1;
    v1._x = _x + obj._x;
    v1._y = _y + obj._y;
    v1._z = _z + obj._z;
    return v1;
}

Vec3 Vec3::operator * (double const obj) {
    Vec3 v;
    v._x = _x * obj;
    v._y = _y * obj;
    v._z = _z * obj;
    return v;
}

Vec3 Vec3::operator / (double const obj) {
    Vec3 v;
    v._x = _x / obj;
    v._y = _y / obj;
    v._z = _z / obj;
    return v;
}

Vec3 & Vec3::operator += (Vec3 const &obj) {

    _x += obj.Getx();
    _y += obj.Gety();
    _z += obj.Getz();
    return *this;
}

Vec3 & Vec3::operator -= (Vec3 const &obj) {

    _x -= obj.Getx();
    _y -= obj.Gety();
    _z -= obj.Getz();
    return *this;
}

Vec3 & Vec3::operator *= (double const obj) {

    _x *= obj;
    _y *= obj;
    _z *= obj;
    return *this;
}

Vec3 & Vec3::operator /= (double const obj) {

    _x /= obj;
    _y /= obj;
    _z /= obj;
    return *this;
}

bool  Vec3::operator == (Vec3 const &obj) {

    if(_x == obj.Getx() && _y == obj.Gety() && _z == obj.Getz())
        return true;
    else
        return false;
}

bool  Vec3::operator != (Vec3 const &obj) {

    if(_x != obj.Getx() && _y != obj.Gety() && _z != obj.Getz())
        return true;
    else
        return false;
}

Vec3 Vec3::operator * (Vec3 const &obj) {

    Vec3 v;
    v._x = _x * obj.Getx();
    v._y = _y * obj.Gety();
    v._z = _z * obj.Getz();
    return v;
}

double Vec3::operator [] (int const obj) {

    if(1 == obj)
    {
        cout<<"You entered X coordinate of vector that is "<<_x<<"."<<endl;
        return _x;
    }
    else if(2 == obj)
    {
        cout<<"You entered Y coordinate of vector that is "<<_y<<"."<<endl;
        return _y;
    }
    else if(3 == obj)
    {
        cout<<"You entered Z coordinate of vector that is "<<_z<<"."<<endl;
        return _z;
    }
    else
        cout<<"That is not coordinate of this vector."<<endl;

}

Vec3 Vec3::Clan() {
    Vec3 v;
    double lenght = sqrt(Getx() * Getx() + Gety() * Gety() + Getz() * Getz());
    v._x = _x / lenght;
    v._y = _y / lenght;
    v._z = _z / lenght;
    cout<<"X norm. : "<<v._x<<endl<<"Y norm. : "<<v._y<<endl<<"Z norm. : "<<v._z<<endl;
    return v;
}


