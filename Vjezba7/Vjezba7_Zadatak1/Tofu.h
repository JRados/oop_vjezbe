#ifndef TOFU_H
#define TOFU_H
#include "Vege.h"

class Tofu : public Vege{
public:
    Tofu(double SamostalnoJelo, double DioDrugogJela, string type, string name,
         double water, double protein, double fat, double carbs, string date, double AmountPerDay);
};

#endif // TOFU_H
