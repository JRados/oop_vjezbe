#ifndef CHEESE_H
#define CHEESE_H
#include "Dairy.h"

class Cheese : public Dairy{
public:
    Cheese(double SamostalnoJelo, double DioDrugogJela, string type, string name, double water, double protein, double fat, double carbs, string date, double AmountPerDay);
};
#endif // CHEESE_H
