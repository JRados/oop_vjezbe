#ifndef MADJARICA_H
#define MADJARICA_H
#include "Dessert.h"

class Madjarica : public Dessert{
public:
    Madjarica(double SamostalnoJelo, string type, string name, double water, double protein, double fat, double carbs, string date, double AmountPerDay);

};
#endif // MADJARICA_H
