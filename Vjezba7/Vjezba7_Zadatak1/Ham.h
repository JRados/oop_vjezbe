#ifndef HAM_H
#define HAM_H
#include "Meat.h"
class Ham : public Meat{
public:
    Ham(double SamostalnoJelo, double DioDrugogJela, string type, string name, double water, double protein, double fat, double carbs, string date, double AmountPerDay);
};
#endif // HAM_H
