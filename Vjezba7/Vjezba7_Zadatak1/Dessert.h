#ifndef DESSERT_H
#define DESSERT_H
#include<iostream>
#include "Food.h"

class Dessert : public Food{
protected:
    double _SamostalnoJelo;

public:
    Dessert(double SamostalnoJelo, string type, string name,
             double water, double protein, double fat, double carbs, string date, double AmountPerDay);

    virtual void print(std::ostream& os) const
		{
			os << "Ime: " << _name << endl;
			os << "Potrosnja kao jelo: " << _SamostalnoJelo << " " << "kg" << endl;
		}
    friend istream & operator>>(istream & is, Dessert& m) {
			is >> m._SamostalnoJelo
				>> m._type >> m._name
				>> m._water >> m._protein >> m._fat
				>> m._carbs >> m._endDate >> m._AmountPerDay;
			return is;
		}



};

#endif // DESSERT_H
