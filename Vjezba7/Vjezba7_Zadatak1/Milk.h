#ifndef MILK_H
#define MILK_H
#include "Dairy.h"

class Milk : public Dairy{
public:
    Milk(double SamostalnoJelo, double DioDrugogJela, string type, string name, double water, double protein, double fat, double carbs, string date, double AmountPerDay);

};
#endif // MILK_H
