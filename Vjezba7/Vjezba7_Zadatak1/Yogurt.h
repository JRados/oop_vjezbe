#ifndef YOGURT_H
#define YOGURT_H
#include "Dairy.h"

class Yogurt : public Dairy{
public:
    Yogurt(double SamostalnoJelo, double DioDrugogJela, string type, string name, double water, double protein, double fat, double carbs, string date, double AmountPerDay);
};
#endif // YOGURT_H
