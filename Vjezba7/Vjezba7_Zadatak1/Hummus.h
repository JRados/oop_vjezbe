#ifndef HUMMUS_H
#define HUMMUS_H
#include "Vege.h"

class Hummus : public Vege{
public:
    Hummus(double SamostalnoJelo , double DioDrugogJela, string type, string name, double water, double protein, double fat, double carbs, string date, double AmountPerDay);
};
#endif // HUMMUS_H
