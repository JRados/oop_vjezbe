#ifndef RAWMEAT_H
#define RAWMEAT_H
#include "Meat.h"
class RawMeat : public Meat{
public:
    RawMeat(double SamostalnoJelo , double DioDrugogJela , string type, string name, double water, double protein, double fat, double carbs, string date, double AmountPerDay);

};
#endif // RAWMEAT_H
