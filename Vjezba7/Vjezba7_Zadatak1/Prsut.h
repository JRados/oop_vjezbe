#ifndef PRSUT_H
#define PRSUT_H
#include "Meat.h"
class Prsut : public Meat{
public:
    Prsut(double SamostalnoJelo, double DioDrugogJela, string type, string name, double water, double protein, double fat, double carbs, string date, double AmountPerDay);

};
#endif // PRSUT_H
