#include "Food.h"
#include <iostream>
#include <string>
#include <ctime>
#include <algorithm>
#include <new>

Food::Food(string type, string name, double water, double protein, double fat, double carbs, string date, double AmountPerDay)
{
    _type = type;
    _name = name;
    _water = water;
    _protein = protein;
    _fat = fat;
    _carbs = carbs;
    _endDate = date;
    _AmountPerDay = AmountPerDay;
}

Food::Food(const Food &p2)
{

    _type = p2._type;
    _name = p2._name;
    _water = p2._water;
    _protein = p2._protein;
    _fat = p2._fat;
    _carbs = p2._carbs;
    _endDate = p2._endDate;
    _AmountPerDay = p2._AmountPerDay;
    _ArraySize = p2._ArraySize;
    //_monthly = new MonthlyAmount[p2._ArraySize];
    //copy(p2._monthly, p2._monthly + _ArraySize, _monthly);
}


const int MonthlyAmount::GetYear()
{
    return year;
}
const int MonthlyAmount::GetMonth()
{
    return month;
}
const double MonthlyAmount::GetAmount()
{
    return amount;
}
string Food::GetName()
{
    return _name;
}

void Food::SetFood(string type, string name, double water, double protein, double fat, double carbs, string date, double AmountPerDay)
{
    _type = type;
    _name = name;
    _water = water;
    _protein = protein;
    _fat = fat;
    _carbs = carbs;
    _endDate = date;
    _AmountPerDay = AmountPerDay;
}


void MonthlyAmount::InnerSet(double k, int y, int m)
{
    amount = k;
    month = m;
    year = y;
}

int Food::GetCurrentYear()
{
    time_t t = time(0);
    tm *now = localtime(&t);
    return 1900 + now->tm_year;
}

int Food::GetCurrentMonth()
{
    time_t t = time(0);
    tm *now = localtime(&t);
    return now->tm_mon;
}

int Food::GetEndYear()
{
    int year = 0;
    for(int i=6 ; i<10 ; i++)
    {
        year += _endDate[i] - '0';
        year *= 10;
    }
    year /= 10;
    return year;
}

double Food::SetRandValue(int low, int high)
{
    return low + static_cast <double> (rand()) / (static_cast <double> (RAND_MAX/ (high - low)));
}


void Food::SetArrayMonthly()
{
    int CYear = GetCurrentYear();
    int CMonth = GetCurrentMonth();
    CMonth++;

    _ArraySize = (GetEndYear() - CYear) * 24;
    _monthly = new MonthlyAmount [_ArraySize];

    for(int i=0 ; i < _ArraySize ; i++)
    {
        _monthly[i].InnerSet(SetRandValue(0,(rand() % 200 + 10)),CYear,CMonth);
        if(CMonth == 12)
        {
            CMonth = 0;
            CYear ++;
        }
        CMonth++;
    }
}

const MonthlyAmount Food::GetMonthly(int m)
{
    return
    _monthly[m];
    //MALO PROUCIT ODI POINTER KAKO STA ZASTO
}

void Food::ChangeAmountPerDay(bool flag)
{
    if(flag)
        _AmountPerDay ++;
    else
        _AmountPerDay --;
}

const double Food::GetAmountPerDay()
{
    return _AmountPerDay;
}

void Food::SetMonthly(double amount,int y, int m)
{
    for(int i = 0 ; i<_ArraySize ; i++)
    {
        if(_monthly[i].GetYear() == y && _monthly[i].GetMonth() == m && y == GetCurrentYear())
        {
            if(_monthly[i].GetAmount() != 0)
            {
                cout<<"Existing data in current year of that month:"<<endl<<"Year - "<<_monthly[i].GetYear()<<endl<<"Month - "<<_monthly[i].GetMonth()<<endl<<"Amount - "<<_monthly[i].GetAmount()<<endl<<"Do you want to change it?"<<endl<<"1-Yes   2-No"<<endl;
                int temp;
                do
                {
                    cin>>temp;
                }while(temp != 0 && temp != 1);

                if(temp)
                {
                    _monthly[i].InnerSet(amount, y, m);
                    return;
                }
                else
                    return;
            }
            else
            {
                _monthly[i].InnerSet(amount, y, m);
                return;
            }
        }
    }
    cout<<"Data can't be changed or entered."<<endl;
}

double Food::DetectPercent(int year)
{
     double PYear = 0, CYear = 0, Percent;

     for(int i = 0 ; i < _ArraySize ; i++)
     {
         if(_monthly[i].GetYear() == year)
         {
             CYear += _monthly[i].GetAmount();
         }
     }

     year--;

     for(int i = 0 ; i < _ArraySize ; i++)
     {
         if(_monthly[i].GetYear() == year)
         {
             PYear += _monthly[i].GetAmount();
         }
     }

     return CYear / PYear;
}

void Food::ChangeDailyAmount(int year)
{
    if(DetectPercent(year) > 1)
    {
        cout<<_AmountPerDay<<endl;
        cout<<"It was sell "<<(DetectPercent(2020) - 1) * 100<<"% less this year than last year."<<endl;
        _AmountPerDay -= _AmountPerDay *0.1;
        cout<<_AmountPerDay<<endl<<endl;
    }
    else
    {
        cout<<_AmountPerDay<<endl;
        cout<<"It was sell "<<DetectPercent(2020) * 100<<"% more this year than last year."<<endl;
        _AmountPerDay += _AmountPerDay *0.1;
        cout<<_AmountPerDay<<endl<<endl;
    }


}

const void Food::PrintClass()
{
    cout<<"Type:    "<<_type<<endl;
    cout<<"Name:    "<<_name<<endl;
    cout<<"Water:   "<<_water<<endl;
    cout<<"Protein: "<<_protein<<endl;
    cout<<"Fat:     "<<_fat<<endl;
    cout<<"Carbs:   "<<_carbs<<endl;
    cout<<"EndDate: "<<_endDate<<endl;
    cout<<"APD:     "<<_AmountPerDay<<endl;
    cout<<"Monthly Amount"<<endl<<endl;
    cout<<"YEAR MONTH   AMOUNT"<<endl;

    for(int i=0 ; i<_ArraySize ; i++)
    {
        cout<<_monthly[i].GetYear()<<"  "<<_monthly[i].GetMonth()<<"    "<<_monthly->GetAmount()<<endl;
    }
}


Food::~Food()
{
    delete []_monthly;
}
