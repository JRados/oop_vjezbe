#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include "Food.h"
#include "Meat.h"
#include "Prsut.h"
#include "Ham.h"
#include "RawMeat.h"
#include "Dairy.h"
#include "Milk.h"
#include "Cheese.h"
#include "Yogurt.h"
#include "Vege.h"
#include "Hummus.h"
#include "Tofu.h"
#include "Rice.h"
#include "Dessert.h"
#include "Krempita.h"
#include "Madjarica.h"
#include "Cake.h"

using namespace std;

int Rand()
{
    int x = rand() % 1000 +10;
}

int main()
{
    /*vector<Food> Restoraunt ;
    Restoraunt.assign(5,Food());
    srand(static_cast <unsigned> (time(nullptr)));

    //PUNJENJE POLJA
    Restoraunt[0].SetFood("Pita","Pita od sira", 102.5, 12.0, 24.4, 35.2, "13-05-2021", 10.2);
    Restoraunt[1].SetFood("Jelo","Manistra", 205.3, 22.0, 44.4, 20.2, "13-05-2021", 40.5);
    Restoraunt[2].SetFood("Pita","Pita od mesa", 122.5, 32.0, 24.4, 31.2, "13-05-2021", 12.2);
    Restoraunt[3].SetFood("Jelo","Odrezak", 202.5, 18.0, 24.4, 40.2, "13-05-2021", 30.2);
    Restoraunt[4].SetFood("Prilog","Pire", 302.5, 16.0, 34.4, 41.2, "13-05-2021", 31.2);

    //NIZ I PROMJENA DNEVNE POTROSNJE U SLUCAJU PORASTA ILI PADA PRODAJE
    for(int i = 0; i<5; i++)
    {
        Restoraunt[i].SetArrayMonthly();
        Restoraunt[i].ChangeDailyAmount(2020);

    }

    //ISPIS OBJEKTA I DESTRUKTOR
    for(int i = 0 ; i<5 ; i++)
    {
        Restoraunt[i].PrintClass();
        Restoraunt[i].~Food();
    }*/

    vector<Food*> Restoran;
	Prsut p(1, 40, "Meat", "Prsut1", 1, 2, 3, 4, "01-01-2020", 1);
	Restoran.push_back(&p);
	RawMeat rm(2, 50, "Meat", "RMeat1", 1, 2, 3, 4, "01-01-2020", 2);
	Restoran.push_back(&rm);
	Ham h(3, 60, "Meat", "Ham1", 1, 2, 3, 4, "01-01-2020", 3);
	Restoran.push_back(&h);
	Yogurt j(4, 70, "Dairy", "Jogurt1", 1, 2, 3, 4, "01-01-2020", 4);
	Restoran.push_back(&j);
	Cheese ch(5, 80, "Dairy", "Sir1", 1, 2, 3, 4, "01-01-2020", 5);
	Restoran.push_back(&ch);
	Milk mi(90, 5, "Dairy", "Milky", 1, 2, 3, 4, "01-01-2020", 6);
	Restoran.push_back(&mi);
	Krempita kr(100, "Dessert", "Kolac1", 1, 2, 3, 4, "01-01-2020", 7);
	Restoran.push_back(&kr);
	Madjarica ma(110, "Dessert", "Kolac2", 1, 2, 3, 4, "01-01-2020", 8);
	Restoran.push_back(&ma);
	Cake ck(140, "Dessert", "Kolac3", 1, 2, 3, 4, "01-01-2020", 11);
	Restoran.push_back(&ck);
	Rice r(6, 120, "Vege", "Vege1", 1, 2, 3, 4, "01-01-2020", 9);
	Restoran.push_back(&r);
	Tofu t(7, 130, "Vege", "Vege2", 1, 2, 3, 4, "01-01-2020", 10);
	Restoran.push_back(&t);
	Hummus hm(8, 150, "Vege", "Vege3", 1, 2, 3, 4, "01-01-2020", 12);
	Restoran.push_back(&hm);

	vector<Food*>::iterator it;
	float total = 0.0;

	for (int i = 0; i < Restoran.size(); i++) {
		total = total + Restoran[i]->GetAmountPerDay();
		cout << *Restoran[i] << endl;
	}

	cout << "ukupna potrosnja: " << total << endl;

}
