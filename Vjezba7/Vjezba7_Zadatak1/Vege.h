#ifndef VEGE_H
#define VEGE_H
#include"Food.h"
#include<string>
#include<iostream>
class Vege : public Food{
protected:
    double _SamostalnoJelo;
    double _DioDrugogJela;

public:
    Vege(double SamostalnoJelo, double DioDrugogJela, string type, string name, double water,
         double protein, double fat, double carbs, string date, double AmountPerDay);

    virtual void print(std::ostream& os) const
		{
			os << "Ime: " << _name << endl;
			os << "Potrosnja samostalnog jela: " << _SamostalnoJelo << " " << "kg" << endl;
			os << "Potorsnja kao dio drugog jela: " << _DioDrugogJela << " " << "kg" << endl;
		}

		friend istream & operator>>(istream & is, Vege& m) {
			is >> m._SamostalnoJelo
				>> m._DioDrugogJela >> m._type >> m._name
				>> m._water >> m._protein >> m._fat
				>> m._carbs >> m._endDate >> m._AmountPerDay;
			return is;
		}


};
#endif // VEGE_H
