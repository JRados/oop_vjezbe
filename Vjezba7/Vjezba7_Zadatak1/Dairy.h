#ifndef DAIRY_H
#define DAIRY_H
#include<string>
#include<iostream>
#include"Food.h"
class Dairy : public Food {

protected:
    double _SamostalnoJelo;
    double _DioDrugogJela;

public:
    Dairy(double SamostalnoJelo, double DioDrugogJela, string type, string name, double water,
          double protein, double fat, double carbs, string date, double AmountPerDay);

    virtual void print(std::ostream& os) const
		{
			os << "Ime: " << _name << endl;
			os << "Potrosnja samostalnog jela: " << _SamostalnoJelo << " " << "kg" << endl;
			os << "Potorsnja kao dio drugog jela: " << _DioDrugogJela << " " << "kg" << endl;
		}

		friend istream & operator>>(istream & is, Dairy& m) {
			is >> m._SamostalnoJelo
				>> m._DioDrugogJela >> m._type >> m._name
				>> m._water >> m._protein >> m._fat
				>> m._carbs >> m._endDate >> m._AmountPerDay;
			return is;
		}


};
#endif // DAIRY_H
