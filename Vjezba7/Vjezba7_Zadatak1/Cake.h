#ifndef CAKE_H
#define CAKE_H
#include "Dessert.h"

class Cake : public Dessert{
public:
    Cake(double SamostalnoJelo,  string type, string name, double water, double protein, double fat, double carbs, string date, double AmountPerDay);
};
#endif // CAKE_H
