#ifndef RICE_H
#define RICE_H
#include "Vege.h"

class Rice : public Vege{
public:
    Rice(double SamostalnoJelo, double DioDrugogJela, string type, string name, double water, double protein, double fat, double carbs, string date, double AmountPerDay);

};
#endif // RICE_H
