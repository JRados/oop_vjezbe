#ifndef MEAT_H
#define MEAT_H
#include<iostream>
#include<string>
#include "Food.h"
class Meat : public Food
{
protected:
        double _SamostalnoJelo;
        double _DioDrugogJela;
public:
        Meat(double SamostalnoJelo , double DioDrugogJela, string type, string name, double water, double protein,
              double fat, double carbs, string date, double AmountPerDay);

        virtual void print(std::ostream& os) const
		{
			os << "Ime: " << _name << endl;
			os << "Potrosnja samostalnog jela: " << _SamostalnoJelo << " " << "kg" << endl;
			os << "Potorsnja kao dio drugog jela: " << _DioDrugogJela << " " << "kg" << endl;
		}

		friend istream & operator>>(istream & is, Meat& m) {
			is >> m._SamostalnoJelo
				>> m._DioDrugogJela >> m._type >> m._name
				>> m._water >> m._protein >> m._fat
				>> m._carbs >> m._endDate >> m._AmountPerDay;
			return is;
		}




};

#endif // MEAT_H
