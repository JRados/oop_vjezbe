#ifndef KREMPITA_H
#define KREMPITA_H
#include "Dessert.h"
class Krempita : public Dessert{
public:
    Krempita(double SamostalnoJelo, string type, string name, double water, double protein, double fat, double carbs, string date, double AmountPerDay);

};
#endif // KREMPITA_H
