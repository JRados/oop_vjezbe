#include <iostream>
#include <string>
#include <new>
#include <time.h>
#include <vector>
#include <iomanip>
#include <string.h>


using namespace std;

typedef struct{

    string ID;
    string name;
    string gender;
    int quiz_1;
    int quiz_2;
    int mid_grade;
    int final_grade;
    int total_grade;
}Student;


void add_new(vector<Student> &razred, int &N)
{
    fflush(stdin);
    Student temp;
    string a;
    cout<<"Unesi ID:    "<<endl;
    getline(cin,temp.ID);

    for(int i = 0 ; i<N ; i++)
    {
        if(temp.ID == razred[i].ID)
        {
            cout<<"ID vec postoji!"<<endl;
            return;
        }
    }

    cout<<"Unesi Ime:    "<<endl;
    getline(cin,temp.name);

    cout<<"Unesi spol:    "<<endl;
    getline(cin,temp.gender);

    cout<<"Unesi quiz1:    "<<endl;
    cin>>temp.quiz_1;

    cout<<"Unesi quiz2:    "<<endl;
    cin>>temp.quiz_2;

    cout<<"Unesi mid_grade:    "<<endl;
    cin>>temp.mid_grade;

    cout<<"Unesi final_grade:    "<<endl;
    cin>>temp.final_grade;

    temp.total_grade = temp.final_grade + temp.mid_grade + temp.quiz_2 + temp.quiz_1 ;

    razred.push_back(temp);
    N++;
}

void erase_student(vector<Student> &razred,int  &N, string str)
{
    fflush(stdin);
    for( int i=0 ; i<N ; i++)
    {
        if(str == (razred[i].ID))
        {
            razred.erase(razred.begin() + i);
            N--;
            //cout<<"USPILO"<<endl;
            return;
        }
    }
    cout<<"ID ne postoji!"<<endl;
}

void update(vector<Student> &razred, int N, string str)
{
    fflush(stdin);
    for( int i=0 ; i<N ; i++)
    {
         if(str == (razred[i].ID))
         {
            fflush(stdin);
            cout<<"Unesi Name:    "<<endl;
            getline(cin,razred[i].name);
            cout<<"Unesi Spol:    "<<endl;
            getline(cin,razred[i].gender);
            cout<<"Unesi quiz1:    "<<endl;
            cin>>razred[i].quiz_1;
            cout<<"Unesi quiz2:    "<<endl;
            cin>>razred[i].quiz_2;
            cout<<"Unesi mid:    "<<endl;
            cin>>razred[i].mid_grade;
            cout<<"Unesi finale:    "<<endl;
            cin>>razred[i].final_grade;
            razred[i].total_grade = razred[i].final_grade + razred[i].mid_grade + razred[i].quiz_2 + razred[i].quiz_1 ;
            return;
         }
    }
    cout<<"ID ne postoji!"<<endl;
}

void print(vector<Student> &razred, int N)
{
    fflush(stdin);
    for(int i = 0 ; i < N ; i++ )
    {
        cout<<razred[i].ID<<endl;
        cout<<razred[i].name<<endl;
        cout<<razred[i].gender<<endl;
        cout<<razred[i].quiz_1<<endl;
        cout<<razred[i].quiz_2<<endl;
        cout<<razred[i].mid_grade<<endl;
        cout<<razred[i].final_grade<<endl;
        cout<<razred[i].total_grade<<endl<<endl;
    }
}

void avg(Student stud)
{
    fflush(stdin);
    cout<<"Prosjek studenta je: "<<stud.total_grade / 4<<" bodova"<<endl;
}

void max_point_stud(vector<Student>razred, int N)
{
    fflush(stdin);
    int temp = 0, index;
    for(int i = 0 ; i < N ; i++ )
    {
        if( temp < razred[i].total_grade)
        {
            temp = razred[i].total_grade;
            index = i;
        }
    }

    cout<<"STUDENT S NAJVECIM BROJEM BODOVA"<<endl<<endl;

    cout<<razred[index].ID<<endl;
    cout<<razred[index].name<<endl;
    cout<<razred[index].gender<<endl;
    cout<<razred[index].quiz_1<<endl;
    cout<<razred[index].quiz_2<<endl;
    cout<<razred[index].mid_grade<<endl;
    cout<<razred[index].final_grade<<endl;
    cout<<razred[index].total_grade<<endl<<endl;

}

void min_point_stud(vector<Student>razred, int N)
{
    fflush(stdin);
    int temp = razred[0].total_grade, index;
    for(int i = 0 ; i < N ; i++ )
    {
        if( temp > razred[i].total_grade)
        {
            temp = razred[i].total_grade;
            index = i;
        }
    }

    cout<<"STUDENT S NAJMANJIM BROJEM BODOVA"<<endl<<endl;

    cout<<razred[index].ID<<endl;
    cout<<razred[index].name<<endl;
    cout<<razred[index].gender<<endl;
    cout<<razred[index].quiz_1<<endl;
    cout<<razred[index].quiz_2<<endl;
    cout<<razred[index].mid_grade<<endl;
    cout<<razred[index].final_grade<<endl;
    cout<<razred[index].total_grade<<endl<<endl;

}

int rand_value(int low, int high)
{
    return rand() % high - low ;
}

void ID_search(vector<Student> &razred, int N, string str)
{
    fflush(stdin);
    for( int i=0 ; i<N ; i++)
    {
         if(str == (razred[i].ID))
         {
            cout<<"Student s ID-om: "<<razred[i].ID<<endl;
            cout<<razred[i].name<<endl;
            cout<<razred[i].gender<<endl;
            cout<<razred[i].quiz_1<<endl;
            cout<<razred[i].quiz_2<<endl;
            cout<<razred[i].mid_grade<<endl;
            cout<<razred[i].final_grade<<endl;
            cout<<razred[i].total_grade<<endl<<endl;
            return;
         }
    }
    cout<<"ID ne postoji!"<<endl;
}

int ID_search_return_index(vector<Student> &razred, int N, string str)
{
    fflush(stdin);
    for( int i=0 ; i<N ; i++)
    {
         if(str == (razred[i].ID))
         {
            return i;
         }
    }
    cout<<"ID ne postoji!"<<endl;
}

void sort_total(vector<Student> &razred, int N)
{
    for(int i = 0; i<N ; i++)
    {
        for(int j = i + 1; j<N ; j++)
        {
            if(razred[i].total_grade > razred[j].total_grade)
            {
                Student temp = razred[i];
                razred[i] = razred[j];
                razred[j] = temp;
            }
        }
    }
}
int main()
{
    srand(time(nullptr));
    vector<Student>razred(20);
    int N = 20, x;
    string str;

    for(int i = 0 ; i < 20 ; i++ )
    {
        razred[i].ID = "00000";
        razred[i].name = "Ucenik";
        razred[i].gender = "2019-ta je ...";
        razred[i].quiz_1 = rand_value(1,100);
        razred[i].quiz_2 = rand_value(1,100);
        razred[i].mid_grade = rand_value(1,100);
        razred[i].final_grade = rand_value(1,100);
        razred[i].total_grade = razred[i].final_grade + razred[i].mid_grade + razred[i].quiz_2 + razred[i].quiz_1 ;
    }


    do
    {
        cout<<"Razred od 20 ucenika je ostvario odredene rezultate"<<endl<<"Odabreite operaciju s podatcima:"<<endl;
        cout<<"1 - Dodajte novog ucenika"<<endl<<"2 - Promjenite podatke uceniku(preko ID ucenika)"<<endl;
        cout<<"3 - Izbrisite podatke ucenika(preko ID ucenika)"<<endl<<"4 - Izracunajte prosjek ucenika(preko ID ucenika)"<<endl;
        cout<<"5 - Student s najvecim brojem bodova"<<endl<<"6 - Student s najmanjim brojem bodova"<<endl;
        cout<<"7 - Ispisite studenta preko ID-a"<<endl<<"8 - Ispisi sve podatke o studentima"<<endl<<"9 - Sortiranje po ukupnim brojem bodova"<<endl<<"0 - IZLAZ"<<endl;

        cin>>x;

        switch(x)
        {
            case 1 :
                fflush(stdin);
                add_new(razred,N);
                break;
            case 2 :
                fflush(stdin);
                cout<<"Unesite ID: ";
                getline(cin,str);
                update(razred,N,str);
                break;
            case 3 :
                fflush(stdin);
                cout<<"Unesite ID: ";
                getline(cin,str);
                erase_student(razred, N, str);
                break;
            case 4 :
                fflush(stdin);
                cout<<"Unesite ID: ";
                getline(cin,str);
                avg(razred[ID_search_return_index(razred,N,str)]);
                break;
            case 5 :
                fflush(stdin);
                max_point_stud(razred, N);
                break;
            case 6 :
                fflush(stdin);
                min_point_stud(razred, N);
                break;
            case 7 :
                fflush(stdin);
                cout<<"Unesite ID: ";
                getline(cin,str);
                ID_search(razred, N, str);
                break;
            case 8 :
                fflush(stdin);
                print(razred,N);
                break;
            case 9 :
                fflush(stdin);
                sort_total(razred,N);
                break;
            case 0 :
                break;
            default :
                cout<<"Krivi broj! Upisite ponovno ..."<<endl;

        }



    }while (x != 0);

/*




    add_new(razred,N);
    //erase_student(razred,N,"ja");
    update(razred,N,"ja");
    sort_total(razred, N);
    print(razred,N);
    avg(razred[N-1]);
    //max_point_stud(razred,N);
    //min_point_stud(razred,N);
    //ID_search(razred,N,"aaa");

*/

}
