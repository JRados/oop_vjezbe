#include <iostream>
#include <new>

using namespace std;

int &testiraj(int &x)
{
     int temp1;
     int temp2;

     temp1 = x % 10;
     temp2 = x / 100;
     temp2 = temp2 % 10;
     if(temp1 + temp2 == 5)
        return ++x;
     else
        return x;
}


void pronadi(int *niz, int n)
{

    for(int i=0 ; i<n ; i++)
    {
        niz[i] = testiraj(niz[i]);
    }
}


int main()
{
    int niz[4];
    int x;

    niz[0] = 3287;
    niz[1] = 3283;
    niz[2] = 3587;
    niz[3] = 3181;

    for(int i=0; i<4 ; i++)
    {
        cout<<niz[i]<<endl;
    }

    pronadi(niz,4);
    cout<<endl<<endl;

    for(int i=0; i<4 ; i++)
    {
        cout<<niz[i]<<endl;
    }

}
