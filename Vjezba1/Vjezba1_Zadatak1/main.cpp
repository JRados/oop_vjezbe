#include <iostream>

using namespace std;

int main()
{
    int *niz;
    niz = new int[9];

    for(int i=0;i<9;i++)
    {
        niz[i] = i+1;
    }

    niz[5]=5;
    niz[6]=5;
    niz[7]=2;
    niz[8]=2;

    for(int i=0;i<9;i++)
    {
        int counter = 0;
        for(int j=i+1;j<9;j++)
        {
            if(niz[i] == niz[j])
            {
                counter++;
                niz[j]=0;
            }
        }
        if(counter != 0 && niz[i] != 0)
           cout<<"U nizu se nalazi "<<counter<<" broj duplikata broja "<<niz[i]<<endl;
    }

}
