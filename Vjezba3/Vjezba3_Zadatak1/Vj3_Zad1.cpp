#include<iostream>
#include<vector>
#include<new>
#include<time.h>



using namespace std;

vector<int> upis_vrijednosti(int a=0, int b=100, int n=5, bool var = true)
{
    vector<int> v;

    if (var == true)
    {
        v.assign(n, (rand() % b + a));
    }
    return v;
}

void ispisi_vektor(vector<int> v)
{
    for(int i=0;i<v.size();i++)
    {
        cout<<v[i]<<endl;
    }
}

int main()
{
    srand (time(NULL));
    vector<int> vec = upis_vrijednosti();
    ispisi_vektor(vec);
}
