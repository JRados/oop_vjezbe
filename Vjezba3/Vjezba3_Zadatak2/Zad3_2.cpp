#include<iostream>
#include<new>
using namespace std;



typedef struct{
    int *niz;
    int phy_size;
    int log_size = 0;

    void vektor_new(int n)
    {
        niz = new int[n];
        phy_size = n;
    }

    void vektor_delete()
    {
        delete niz;
    }

    void vektor_push_back(int value)
    {
        if(phy_size == log_size)
        {
            phy_size *= 2;
            niz = (int*) realloc (niz, sizeof(int) * phy_size);
        }
        niz[log_size] = value;
        log_size++;
    }

    void vektor_pop_back()
    {
        niz[log_size - 1] = 0;
        log_size --;
    }

    int& vektor_front()
    {
        return niz[0];
    }

    int& vektor_back()
    {
        return niz[log_size-1];
    }

    int& vektor_size()
    {
        return log_size;
    }


} vektor;



int main()
{
    vektor v;
    v.vektor_new(3);
    v.vektor_push_back(100);
    v.vektor_push_back(200);
    v.vektor_push_back(300);
    for(int i=0; i<v.log_size;i++)
    {
        cout<<v.niz[i]<<"  ";
    }
    cout<<endl;
    v.vektor_pop_back();
    cout<<v.vektor_front()<<endl;
    cout<<v.vektor_back()<<endl;
    cout<<v.vektor_size()<<endl;
    v.vektor_delete();
}
