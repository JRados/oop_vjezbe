#include<iostream>
#include<string>
#include<vector>
using namespace std;

typedef struct{
    string name;
    string movie;
    int year;
}producent;

typedef struct{
    string name;
    int no_movies;
}producent2;

void producent_movies(vector <producent2> &no, vector <producent> prod)
{
    int len = prod.size();
    producent2 new_p;
    new_p.name = prod[0].name;
    new_p.no_movies = 1;
    no.push_back(new_p);

    for(int i=1 ; i<len ; i++)
    {
        int j;
        int len2 = no.size();
        for(j=0; j<len2 ; j++)
        {
            if(prod[i].name == no[j].name)
            {
                no[j].no_movies ++;
                break;
            }
        }
        if(j == len2)
        {
            new_p.name = prod[i].name;
            new_p.no_movies = 1;
            no.push_back(new_p);
        }
    }
}

void most_movies(vector <producent2> no)
{
    int max = 0;
    int len = no.size();

    for(int i=0; i<len ; i++)
    {
        if(no[i].no_movies > max)
            max = no[i].no_movies;
    }

    for(int i=0; i<len ; i++)
    {
        if(no[i].no_movies == max)
            cout<<"Producent "<<no[i].name<<" has "<<no[i].no_movies<<" movies."<<endl;
    }
}


int main()
{
    vector<producent> prod;
    vector<producent2> no;
    producent new_prod;

    new_prod.movie = "m1";
    new_prod.name = "n1";
    new_prod.year = 1995;
    prod.push_back(new_prod);

    new_prod.movie = "m2";
    new_prod.name = "n1";
    new_prod.year = 1995;
    prod.push_back(new_prod);

    new_prod.movie = "m3";
    new_prod.name = "n2";
    new_prod.year = 1995;
    prod.push_back(new_prod);

    new_prod.movie = "m4";
    new_prod.name = "n3 ";
    new_prod.year = 1995;
    prod.push_back(new_prod);

    new_prod.movie = "m5";
    new_prod.name = "n2";
    new_prod.year = 1995;
    prod.push_back(new_prod);

    producent_movies(no,prod);
    most_movies(no);

//    destroy(no);
  //  destroy(prod);

    //cout<<prod[4].name;
}
