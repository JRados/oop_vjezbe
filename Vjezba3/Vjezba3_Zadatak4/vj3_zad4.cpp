#include<iostream>
#include<time.h>

using namespace std;

//NASUMICNO GENERIRAN BROJ [1,2,3] ZA PRVI POTEZ RACUNALA I POTEZ KADA JE RACUNALO U GUBITNOJ POZICIJI
int first_move()
{
    return rand() % (3+1 - 1) + 1 ;
}

void game()
{
    int matches = 21, test;

    //FIRST TURN - COMPUTER
    cout<<"The Computer makes first turn."<<endl;
    int x = first_move();
    matches -= x;
    cout<<"Computer takes "<<x<<" matches."<<endl<<"There is left "<<matches<<" matches."<<endl;

    //FIRST TURN - PLAYER
    do
    {
        cout<<endl<<"Your turn!"<<endl<<"How many matches will you take?"<<endl;
        cin>>x;
    }while(x>3 || x<1);

    matches -=x;
    cout<<"You take "<<x<<" matches."<<endl<<endl<<"There is left "<<matches<<" matches"<<endl<<endl;

    //NAKON PRVOG ODIGRANOG KRUGA DOLAZI DO PROVJERE BROJA KADA JE RACUNALO NA REDU PO FORMULI (matches-1)%4 STO JE
    //IZVEDENICA FORMULE m*4 + 1 DA BI DOBILI BROJ SIBICA KOJE RACUNALO UZIMA
    while(matches > 0)
    {
         //RACUNALO
         cout<<"Computer's turn!"<<endl;
         test = (matches - 1) % 4;

         if(matches == 1)
         {
             matches -= 1;
             cout<<endl<<"Computer takes last match, congratulation you WIN!!!"<<endl;
             break;
         }

         if ( test == 0 )
         {
             x = first_move();
             matches -= x;
             cout<<"Computer takes "<<x<<" matches."<<endl<<endl<<"There is left "<<matches<<" matches."<<endl<<endl;
         }

         else
         {
             matches -= test;
             cout<<"Computer takes "<<test<<" matches."<<endl<<endl<<"There is left "<<matches<<"matches."<<endl<<endl;
         }

         //IGRAC - UVIJET MORA BITI DA SE NESMIJE UNIJETI BROJ MANJI ILI JEDNAK TRENUTNOM STANJU SIBICA I DA BROJ MORA BITI [1,2,3]
         if(matches == 1)
         {
             cout<<endl<<"You have to take last match, you LOST!"<<endl;
         }
         else
         {
             do
             {
                 cout<<"Your turn!"<<endl<<"How many matches will you take?"<<endl;
                 cin>>x;
             }while(x>=matches || (x >3 || x<1));
         }
         matches -= x;
         cout<<"You take "<<x<<" matches"<<endl<<endl<<"There is left "<<matches<<" matches"<<endl<<endl;
    }
}

int main()
{
    int play = 1;
    srand(time(NULL));
    do
    {
        game();
        cout<<endl<<endl<<"Do you want to play again?\t1-YES\t2-NO"<<endl;
        cin>>play;
        cout<<endl;
    }while(play == 1);
    cout<<"Thanks for playing!"<<endl;

}
