#include<iostream>
#include<string>
#include<vector>

using namespace std;

void string_to_numbers(string &str)
{
    int len = str.length();
    char C;
    for(int i=0 ; i<len ; i++)
    {
        C = str[i];
        int j=1;
        while(str[i+j] == str[i])
        {
            j++;
        }
        if(j != 1)
        {
            str.erase(str.begin() + (i + 1), str.begin() + (i + j));
            char temp = j;
            str.insert(i,1,temp);
            i+=1;
        }
    }
}

string numbers_to_string(string &str)
{
    int len = str.length();
    char C;
    for(int i=0 ; i<len ; i++)
    {
        C = str[i];
        if(isdigit(str[i]))
        {
            int temp = int(C);
            str.insert(i+1,temp,str[i+1]);
            str.erase(str.begin()+i);
        }
    }
    return str;
}

void enter_strings(vector <string> &v_str, int N)
{
    string str;
    for(int i=0 ; i<N ; i++)
    {
        int flag = 0;
        do
        {
            cout<<"Unesite "<<i+1<<". string:"<<endl;
            //getline(cin, str);
            cin>>str;
            int len = str.length();
            int j;
            for(j = 0 ; j<len ; j++)
            {
                if (!(isalpha(str[j]) && isupper(str[j]) || !isdigit(str[j]) ))
                {break;}
            }
            if(j == len)
                flag = 1;
        }while(flag == 0);
        v_str.push_back(str);
    }
}

void convert_strings(vector <string> v_str, int N)
{
    for(int i=0 ; i<N ; i++)
    {
        int len = v_str[i].length();
        int j;
        for(j = 0 ; j<len ; j++)
        {
            string string1 = v_str[i];
            if(isalpha(string1[j]))
            {}
            else
                break;
        }
        if(j == len)
            string_to_numbers(v_str[i]);
        else
          v_str[i] = numbers_to_string(v_str[i]);

    }
}
int main()
{
    vector <string> v_str;
    int N;

    cout<<"Unesite koliko stringova ce sadrzavati vas vektor"<<endl;
    cin>>N;

    enter_strings(v_str,N);
    convert_strings(v_str,N);

    cout<<v_str[0];
}
