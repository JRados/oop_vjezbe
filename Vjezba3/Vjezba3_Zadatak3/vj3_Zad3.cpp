#include<iostream>
#include<string>

using namespace std;

void enter_string(string &str)
{
    cout<<"Unesite recenicu:"<<endl;
    getline(cin, str);
}

void correct_string(string &str)
{
    int len = str.length();
    char previous = str[0];
    for(int i=1 ; i<len ; i++)
    {
        if (ispunct(str[i]))
        {
            if(str[i-1] == ' ' && str[i+1] != ' '){
                    str.erase (str.begin() + (i-1));
                    str.insert ((i)," ");

            }
            if(str[i-1] == ' ' && str[i+1] == ' '){
                    str.erase(str.begin() + (i+1));
                    str.erase (str.begin() + (i-1));
                    str.insert ((i)," ");}

            if(str[i-1] != ' ' && str[i+1] != ' ')
            {
                str.insert((i+1)," ");
            }
        }
    }
}

int main()
{
    string str;
    enter_string(str);
    correct_string(str);
    cout<<endl<<str;
}
