#include<iostream>
#include<string>
#include<vector>
#include<fstream>
#include<sstream>

#include "Element.h"
#include "Project.h"
#include "PnPMachine.h"

using namespace std;

/***********************************************************KONSTRUKTOR I DESTRUKTOR************************************************************************/

Pnpmachine::Pnpmachine()
{
    Element el("EMPTY");
    for(int i = 0 ; i < 70 ; i++)
        _machine.push_back(el);
}

Pnpmachine::~Pnpmachine(){}


/*****************************************************************ISPIS STANJA******************************************************************************/

void Pnpmachine::PrintCurrentState()
{
    int i = 1;
    for(vector<Element>::iterator it = _machine.begin() ; it != _machine.end() ; it++, i++)
    {
        if(i%2 == 1)
            cout<<i<<" - "<<(*it).GetName()<<"\t\t";
        else
            cout<<i<<" - "<<(*it).GetName()<<endl;
    }
}

/**********************************************************CHANGE ELEMENT ON MACHINE************************************************************************/

void Pnpmachine::ChangeElementOnMachine(vector<Element> &db)
{
    int SlotNumber,x;
    cout<<"Please enter slot number of element you want change:\t";
    while(!(cin>>SlotNumber) || (SlotNumber<1 || SlotNumber>70)) { cout<<"ERROR: Number between 1 and 70 must be entered!"<<endl; cin.clear(); cin.ignore(132,'\n'); }
    vector<Element>::iterator it = _machine.begin() + SlotNumber - 1;
    cout<<endl<<endl<<"In slot "<<SlotNumber<<" is element "<<it->GetName()<<endl
        <<"Do you want to change it?\t1 - Yes\t2 - No"<<endl;
    while(!(cin>>x) || (x<1 || x>2)) { cout<<"ERROR: Number must be entered!"<<endl; cin.clear(); cin.ignore(132,'\n'); }
    if(x == 1)
    {
        vector<Element>::iterator ittest = ChangeElement(db);
        if(ittest == db.end())
            return;
        (*it).SetElement((*ittest).GetName(), (*ittest).GetFeeder_Width(), (*ittest).GetFeeder_Step(), (*ittest).GetPriority());
        cout<<endl<<*it<<endl;
    }
}

vector<Element>::iterator Pnpmachine::ChangeElement(vector<Element> &db)
{
    string str;
    vector<Element>::iterator itnul = db.end();
    cout<<"Enter name of element you want to set on machine:\t";
    cin>>str;
    if(!Element::TestName(str))
    {
        cout<<endl<<"You entered invalid element ID!"<<endl<<endl;
        return itnul;
    }
    for(vector<Element>::iterator it = db.begin() ; it != db.end() ; it++)
    {
        if(str == it->GetName())
        {
            cout<<endl<<"Element is found in database!"<<endl;
            return it;
        }
    }
    cout<<endl<<"Element is not existing in the database !"<<endl<<"To see elements that are in database go to PRINT ELEMENTS."<<endl<<endl;
    return itnul;
}

/************************************************************ACTIVE PROJECT*********************************************************************************/

void Pnpmachine::ActivateProject()
{
    string ProjectName;
    int pcs;
    vector<Element> EleUsage;
    cout<<"Enter name of the project you want to put active on machine:\t";
    getline(cin, ProjectName);
    Project AP(ProjectName);
    AP.OpenProject();//Opening Project
    cout<<"How many PCS of this project:\t";
    cin>>pcs;
    AP.SetPCS(pcs);
    //SET ELEMENTS ON MACHINE
    vector<Element>PE = AP.GetProjectElements();
    for(vector<Element>::iterator it=PE.begin() ; it != PE.end() ; it++)
    {
        vector<Element>::iterator itmac = _machine.begin() + (*it).GetSlot() - 1;
        (*itmac).SetElement((*it).GetName(), (*it).GetFeeder_Width(), (*it).GetFeeder_Step(), (*it).GetPriority());
    }

    ifstream file_obj("Machine/ElementsUsage.txt", ios::in);//ElementsUsage
    if(file_obj.fail())
    {
        cout<<"Error: Opening file!"<<endl;
        return;
    }
    string line,name;
    int u,flag=0;
    Element el;
    while(getline(file_obj,line))
    {
        stringstream ss(line);
        ss>>name>>u;
        el.SetName(name);
        el.SetUsage(u);
        EleUsage.push_back(el);
    }
    file_obj.close();
    //DODAJE KOLICINU ELEMENATA OD PROJEKTA NA UKUPNU POTROSNJU ELEMENATA
    for(vector<Element>::iterator it = PE.begin() ; it != PE.end() ; it++)
    {
        for(vector<Element>::iterator it2 = EleUsage.begin() ; it2 != EleUsage.end() ; it2++)
        {
            if((*it).GetName() == (*it2).GetName())
            {
                int neo = (*it2).GetUsage() + AP.GetPCS()*(*it).GetQuantity();
                (*it2).SetUsage(neo);
                flag = 1;
                break;
            }
        }
        if(flag == 1)
            flag = 0;
        else //AKO SE ELEMENT NIJE KORISTIA DO SADA DODAJE GA U POTROSNJU
        {
            Element el;
            el.SetName((*it).GetName());
            int sum = AP.GetPCS() * (*it).GetQuantity() ; //STVARALO PROBLEM KAD SAN STAVLJA DIREKTNOP U SETUSAGE()
            el.SetUsage(sum);
            EleUsage.push_back(el);
        }
    }
    //ZAPISUJE NOVE KOLICINE U FILE
    ofstream file_obj2("Machine/ElementsUsage.txt", ios::out);
    for(vector<Element>::iterator it = EleUsage.begin() ; it != EleUsage.end() ; it++)
    {
        file_obj2<<(*it).GetName()<<" "<<(*it).GetUsage()<<endl;
    }
    file_obj2.close();

}

/**************************************************************POTROSNJA ELEMENATA**************************************************************************/

void Pnpmachine::AllElementsUsage()
{
    vector<Element> AllUsage;
    ifstream file_obj("Machine/ElementsUsage.txt", ios::in);
    string line, name;
    Element el;
    int u;
    float all = 0;

    while(getline(file_obj, line))
    {
        stringstream ss(line);
        ss>>name>>u;
        all += u;
        el.SetName(name);
        el.SetUsage(u);
        AllUsage.push_back(el);
    }

    for(vector<Element>::iterator it = AllUsage.begin() ; it != AllUsage.end() ; it++)
    {
        float perc = ((*it).GetUsage() / all) * 100;
        cout<<(*it).GetName()<<endl<<"Elements used until now:\t"<<(*it).GetUsage()
            <<endl<<"In percent:\t"<<perc<<"%"<<endl<<endl;
    }
    cout<<endl<<"Sum of all elements used is:\t"<<all<<endl<<endl;
    file_obj.close();
}
