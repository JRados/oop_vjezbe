#ifndef PNPMACHINE_H
#define PNPMACHINE_H

#include<iostream>
#include<string>
#include<vector>
#include "Element.h"
#include "Project.h"

class Pnpmachine
{
private:
    vector<Element> _machine;

public:
    //KONSTRUKTOR I DESTRUKTOR
    Pnpmachine();
    ~Pnpmachine();

    //PRINT
    void PrintCurrentState();
    void ChangeElementOnMachine(vector<Element> &db);
    static vector<Element>::iterator ChangeElement(vector<Element> &db);
    void ActivateProject();
    static void AllElementsUsage();
};
#endif // PNPMACHINE_H
