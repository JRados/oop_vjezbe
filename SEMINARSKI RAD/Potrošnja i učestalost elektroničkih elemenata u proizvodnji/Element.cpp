#include"Element.h"

#include<iostream>
#include<sstream>
#include<fstream>

#include<string>
#include<vector>
#include<ctype.h>

using namespace std;

/**************************************************************KONSTRUKTORI I DESTRUKTOR********************************************************************/
Element::Element(string name, int feeder_width, int feeder_step, int priority)
{
    _name = name;
    _feeder_width = feeder_width;
    _feeder_step = feeder_step;
    _priority = priority;
    _quantity = 0;
    _slot = 0;
    _usage = 0;
}
Element::Element(const Element &el2)
{
    _name = el2.GetName();
    _feeder_width = el2.GetFeeder_Width();
    _feeder_step = el2.GetFeeder_Step();
    _priority = el2.GetPriority();
    _quantity = el2.GetQuantity();
    _slot = el2.GetSlot();
    _usage = el2.GetUsage();
}
Element::~Element(){}

/***********************************************************************GETERI******************************************************************************/
string Element::GetName() const{return _name;}
int Element::GetFeeder_Width() const{return _feeder_width;}
int Element::GetFeeder_Step() const{return _feeder_step;}
int Element::GetPriority() const{return _priority;}
int Element::GetQuantity() const{return _quantity;}
int Element::GetSlot() const{return _slot;}
int Element::GetUsage() const{return _usage;}

/***********************************************************************SETERI******************************************************************************/
void Element::SetName(string str){_name = str;}
void Element::SetFeeder_Width(int fw){_feeder_width = fw;}
void Element::SetFeeder_Step(int fs){_feeder_step = fs;}
void Element::SetPriority(int p){_priority = p;}
void Element::SetQuantity(int q){_quantity = q;}
void Element::SetSlot(int s){_slot = s;}
void Element::SetUsage(int u){_usage = u;}
void Element::SetElement(string name, int feeder_width, int feeder_step, int priority)
{
    _name = name;
    _feeder_width = feeder_width;
    _feeder_step = feeder_step;
    _priority = priority;
}

/*************************************************************ZAPIS OBJEKATA KLASE U FILE*******************************************************************/
void Element::WriteOneElement()
{
    ofstream file_obj ("DBElementi.txt", ios::app | ios::binary);
    Element obj(this->GetName(),this->GetFeeder_Width(), this->GetFeeder_Step());
    file_obj.write((char*)&obj, sizeof(obj));
    file_obj.close();
}

void Element::Write(vector<Element> &ele)
{
    ofstream file_obj ("DBElementi.txt", ios::out  );
    int fw,fs,p;
    for(vector<Element>::iterator it = ele.begin();it != ele.end();++it)
    {
        string str;
        str = it->GetName();
        fw = it->GetFeeder_Width();
        fs = it->GetFeeder_Step();
        p = it->GetPriority();

        file_obj<<str<<" "<<fw<<" "<<fs<<" "<<p<<endl;
    }
    file_obj.close();
}

/**************************************************************ISPIS ELEMENATA IZ FILEA*********************************************************************/
void Element::Read(vector<Element> &ele)
{
    ifstream file_obj("DBElementi.txt", ios::in );
    Element obj;
    string str, line;
    int fw,fs,p;
    while(getline(file_obj, line))
    {
        stringstream ss(line);
        ss>>str>>fw>>fs>>p;
        obj.SetElement(str,fw,fs,p);
        ele.push_back(obj);
    }
    file_obj.close();
}


/**********************************************KREIRANJE ELEMENTA, BRISANJE ELEMENATA I UPDATE ELEMENTA*****************************************************/
bool Element::CreateElement(vector<Element> &ele)
{
    Element el;
    cout<<"Enter data of new element."<<endl<<endl;
    cin>>el;
    string str = el.GetName();
    if(!(Element::TestName(str)))//TESTIRAM UPIS
    {
        cout<<endl<<"You entered invalid element ID !"<<endl;
        return false;
    }
    else{el.SetName(str);}
    for(vector<Element>::iterator it = ele.begin() ; it != ele.end() ; it++)//TESTIRAM POSTOJANJE ELEMENTA
    {
        if(el.GetName() == it->GetName())
        {
            cout<<endl<<"Element with that ID already exist."<<endl<<"If you want to change that element data go to ELEMENT UPDATE"<<endl;
            return false;
        }
    }
    cout<<el;
    ele.push_back(el);
    Element::Write(ele);//RAZMISLIT DA LI KORISTIT APPEND ZA DODAVANJE ELEMENTA OCE LI MI POBUCAT FILE REDOSLJED
    cout<<"Element is created and added to database."<<endl;
    return true;
}

bool Element::DeleteElement(vector<Element> &ele)
{
    //BOOL JE ZBOG POVRATKA NA UPIS NOVOGA ELEMENTA
    string str;
    cout<<"Enter name of element you want to delete:\t";
    cin>>str;
    if(!Element::TestName(str))
    {
        cout<<endl<<"You entered invalid element ID!"<<endl<<endl;
        return false;
    }
    for(vector<Element>::iterator it = ele.begin() ; it != ele.end() ; it++)
    {
        if(str == it->GetName())
        {
            ele.erase(it);
            Element::Write(ele);
            cout<<endl<<"You have successfully erased element "<<str<<" from database."<<endl;
            return true;
        }
    }
    cout<<endl<<"Element is not existing in the database !"<<endl<<"To see elements that are in database go to PRINT ELEMENTS."<<endl<<endl;
    return false;
}

bool Element::UpdateElement(vector<Element> &ele)
{
    string str;
    int fw, fs;
    cout<<"Enter name of element you want to update:\t";
    cin>>str;
    if(!Element::TestName(str))
    {
        cout<<endl<<"You entered invalid element ID !"<<endl;
        return false;
    }
    for(vector<Element>::iterator it = ele.begin() ; it != ele.end() ; it++)
    {
        if(str == it->GetName())
        {
            cout<<*it;
            cout<<"Enter feeder's new width:\t";
            cin>>fw;
            cout<<"Enter feeder's new and step:\t";
            cin>>fs;
            it->SetFeeder_Width(fw);
            it->SetFeeder_Step(fs);
            Element::Write(ele);
            cout<<endl<<"Update for element "<<str<<" was successful.";
            return true;
        }
    }
    cout<<endl<<"Element is not existing in the database !"<<endl<<"To see elements that are in database go to PRINT ELEMENTS."<<endl<<endl;
    return false;
}

/***********************************************************************TESTERI*****************************************************************************/
bool Element::TestName(string &str)
{
    if(!(isalpha(str[0])))
        return false;
    else
        str[0] = toupper(str[0]);
    for(int i = 1; i<13 ; i++)
    {
        if((i == 1 && str[i] != '.') || (i == 5 && str[i] != '.') || (i == 8 && str[i] != '.')){return false;}
        if( (i != 1 && i != 5 && i != 8) && (!(isdigit(str[i]))) ){return false;}
    }
    return true;
}

/************************************************************************OUTPUT*****************************************************************************/
void Element::Output(vector<Element> ele)
{
    for(vector<Element>::iterator it = ele.begin() ; it != ele.end() ; it++)
    {
        cout<<(*it);
    }
}
