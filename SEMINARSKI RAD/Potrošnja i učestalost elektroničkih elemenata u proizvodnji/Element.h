#ifndef ELEMENT_H
#define ELEMENT_H

#include<iostream>
#include<string>
#include<vector>
using namespace std;

class Element{
private:
    string _name;
    int _feeder_width;
    int _feeder_step;
    int _priority;
    int _usage;
    int _quantity;
    int _slot;
    static int _AllElementsUsage;

    public:
        //KONSTRUKTOR I DESTRUKTOR
        Element(string name = "X.000.00.0000", int feeder_width = 0, int feeder_step = 0, int priority = 0);
        Element(const Element &el2);
        ~Element();

        //GETERI ZA POJEDINE OBJEKTE
        string GetName() const;
        int GetFeeder_Width() const;
        int GetFeeder_Step() const;
        int GetPriority () const;
        int GetQuantity () const;
        int GetSlot () const;
        int GetUsage () const;

        //SETERI ZA POJEDINE OBJEKTE I SETER ZA CIJELI OBJEKT KLASE
        void SetName(string str);
        void SetFeeder_Width(int fw);
        void SetFeeder_Step(int fs);
        void SetPriority(int p);
        void SetQuantity(int q);
        void SetSlot(int s);
        void SetUsage(int u);
        void SetElement(string name, int feeder_width, int feeder_step, int priority);

        //ZAPIS ELEMENATA U FILE
        void WriteOneElement();
        static void Write(vector<Element> &ele);

        //ISPIS ELEMENATA IZ FILEA U VEKTOR
        static void Read(vector<Element> &ele);

        //OPTERECENJE INPUT OUTPUT
        friend ostream & operator<<(ostream &out, Element &obj)
        {
            return out<<endl<<obj._name<<endl<<"Feeder Width/Step: "
               <<obj._feeder_width<<"/"<<obj._feeder_step<<endl<<endl;
        }

        friend istream & operator>>(istream &in, Element &obj)
        {
            cout<<"Enter an element ID (X.000.00.0000):\t";
            in>>obj._name;
            cout<<"Enter width of needed feeder:\t";
            in>>obj._feeder_width;
            cout<<"Enter step of feeder for element:\t";
            in>>obj._feeder_step;
            return in;
        }

        //KREIRANJE, BRISANJE I UPDATE ELEMENTA
        static bool CreateElement(vector<Element> &ele);
        static bool DeleteElement(vector<Element> &ele);
        static bool UpdateElement(vector<Element> &ele);

        //TESTERI ZA UNOSE I POPRATNE METODE
        static bool TestName(string &str);

        //ELEMENTS VECTOR ISPIS
        static void Output(vector<Element> ele);




};

#endif // ELEMENT_H
