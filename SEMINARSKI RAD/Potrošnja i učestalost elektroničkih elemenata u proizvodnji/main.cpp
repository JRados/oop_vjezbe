#include <iostream>
#include <string>
#include<vector>
#include "Element.h"
#include "Project.h"
#include "Menu.h"
#include "PnPMachine.h"

#include<iostream>

using namespace std;

int main()
{
    vector<Element> DBElements;
    int Menu01,Menu02,test;
    Element::Read(DBElements);
    Pnpmachine stroj;

    do
    {
        Menu01 = MainMenu();
        if(Menu01 == 1)
        {
            do
            {
                Menu02 = ElementsMenu();
                switch(Menu02)
                {
                    case 1:
                        Element::Output(DBElements);
                        PressEnter();
                        break;
                    case 2:
                        //MOZDA DAT REPEAT OPCIJU ???
                        Element::CreateElement(DBElements);
                        PressEnter();
                        break;
                    case 3:
                        Element::DeleteElement(DBElements);
                        PressEnter();
                        break;
                    case 4:
                        Element::UpdateElement(DBElements);
                        PressEnter();
                        break;
                }

            }while(Menu02 != 5);
        }
        if(Menu01 == 2)
        {
            do
            {
                Menu02 = ProjectsMenu();
                switch(Menu02)
                {
                    case 1:
                        Project::PrintAllProjectNames();
                        PressEnter();
                        break;
                    case 2:
                    {
                        string name;
                        cout<<"Enter name of the project you want to create:\t";
                        cin.ignore(128, '\n');
                        getline(cin, name);
                        Project new_project(name);
                        new_project.CreateProject(DBElements);
                        PressEnter();
                        break;
                    }
                    case 3:
                        string name;
                        cout<<"Enter name of the project you want to delete:\t";
                        cin.ignore(128,'\n');
                        getline(cin, name);
                        Project::DeleteProject(name);
                        PressEnter();
                        break;
                }

            }while(Menu02 != 4);
        }
        if(Menu01 == 3)
        {
            do
            {
                Menu02 = MachineMenu();
                switch(Menu02)
                {
                    case 1:
                        stroj.PrintCurrentState();
                        PressEnter();
                        break;
                    case 2:
                        stroj.ChangeElementOnMachine(DBElements);
                        PressEnter();
                        break;
                    case 3:
                        cin.ignore(132,'\n');
                        stroj.ActivateProject();
                        PressEnter();
                        break;
                }

            }while(Menu02 != 4);
        }
        if(Menu01 == 4)
        {
            Pnpmachine::AllElementsUsage();
            PressEnter();
        }

    }while(Menu01 != 0);

}
