#include "Project.h"
#include "Element.h"

#include<iostream>
#include<sstream>
#include<fstream>

#include<vector>
#include<string>
#include<ctype.h>

using namespace std;


/************************************************************KONSTRUKTOR I DESTRUKTOR***********************************************************************/
Project::Project(string str){ _ProjectName = str; }
Project::~Project(){}

/*********************************************************************GETERI********************************************************************************/
string Project::GetProjectName() const { return _ProjectName;}
vector<Element> Project::GetProjectElements() const { return _ProjectElements; }
int Project::GetPCS() const { return _pcs; }

/*********************************************************************SETERI********************************************************************************/
void Project::SetProjectName(string str) { _ProjectName = str; }
void Project::SetPCS(int pcs) { _pcs = pcs; }

/*****************************************************KREIRANJE, OTVARANJE I BRISANJE PROJEKTA**************************************************************/
void Project::CreateProject(vector<Element> db)
{
    if(TestProjectName())
    {
        cout<<"Project with that name already exist."<<endl
            <<"Update existing project or give it different name."<<endl;
        cout<<endl<<endl<<"Press Enter to continue ..."<<endl<<endl;
        cin.ignore(132, '\n');
    }
    else
    {
        ofstream file_obj (("Projects/"+_ProjectName+".txt").c_str(), ios::out  );
        if(file_obj.fail())
        {
            cout<<"Error: Opening file!"<<endl;
            return;
        }
        string str;
        bool flag;
        int q,s;
        cout<<"Enter elements to project by their name and quantity of elements for one PCB."<<endl<<"To end input enter 'end'."<<endl<<endl;
        do
        {
            cout<<"Enter element name:\t";
            do{ cin>>str; if(str=="end"){break;} }while(!(Element::TestName(str))); //TEST VALJANOSTI NAZIVA
            for(vector<Element>::iterator it = db.begin() ; it != db.end() ; it++) //PROVJERA NAZIVA U DBELEMENTS
            {
                if(str == it->GetName())
                {
                    flag = true;
                    cout<<"Element is found."<<endl<<endl<<"Enter quantity of element per one device:\t";
                    while(!(cin>>q)) { cout<<"ERROR: Number must be entered!"<<endl; cin.clear(); cin.ignore(132,'\n'); } //INT TEST KORISNO
                    cout<<endl<<"Enter slot number (between 1 and 70):\t";
                    while(!(cin>>s) || (s<1 || s>70)) { cout<<"ERROR: Number between 1 and 70 must be entered!"<<endl; cin.clear(); cin.ignore(132,'\n'); } //INT TEST KORISNO
                    Element el(*it);
                    el.SetQuantity(q);//TRIBA POPRAVIT KONSTRUKTOR DA BI OVO VALJALO
                    el.SetSlot(s);
                    _ProjectElements.push_back(el);
                    cout<<endl<<"Element is successfully added to project."<<endl<<endl<<endl<<endl;
                    break;
                }
            }
            if((!flag) && str != "end"){ cout<<"Element don't exist or you entered wrong element name."<<endl<<endl; } //PROVJERA UNOSA PREKO FLAGA I ENDA
            flag = false;
        }while(str != "end");
        Write(); //IZLAZAK IZ PETLJE I ZAPISIVANJE U FILE
        WriteNOPInFile(); //ZAPISIVANJE NAZIVA PROJJEKTA
        file_obj.close();
    }
}

void Project::OpenProject()
{
    if(TestProjectName())
    {
        Read();
        cout<<endl<<"Project is found and opened."<<endl;
    }
    else
    {
        cout<<endl<<"Project is not found or can't be opened, please see if you entered correct project name."<<endl;
    }
}

void Project::DeleteProject(string name)
{
    vector<string> names;
    int x;
    Project::TakeProjectNamesFromFile(names);
    for(vector<string>::iterator it = names.begin() ; it!=names.end() ; it++)
    {
        if(*it == name)
        {
            cout<<"File with name "<<name<<" is found."<<endl
                <<"Confirm to delete file\t1-YES\t2-NO"<<endl;
            while(!(cin>>x) || (x<1 || x>2)) { cout<<"ERROR: Number must be entered!"<<endl; cin.clear(); cin.ignore(132,'\n'); }
            if(x==1)
            {
                if(remove(("Projects/"+name+".txt").c_str()) != 0)
                {
                    cout<<"Error deleting file!"<<endl;
                    return;
                }
                else
                {
                    names.erase(it);
                    Project::WriteProjectNamesInFile(names);
                    cout<<endl<<endl<<"File is succesfully erased!"<<endl<<endl;
                }
            }
        }
    }


}

/*********************************************************WRITE AND READ OUT FILE***************************************************************************/
void Project::Write()
{
    ofstream file_obj(("Projects/"+_ProjectName + ".txt").c_str(), ios::out );
    if(file_obj.fail())
    {
        cout<<"Error: Opening file!"<<endl;
        return;
    }
    int fw,fs,p,q,s;
    for(vector<Element>::iterator it = _ProjectElements.begin();it != _ProjectElements.end();++it)
    {
        string str;
        str = it->GetName();
        fw = it->GetFeeder_Width();
        fs = it->GetFeeder_Step();
        p = it->GetPriority();
        q = it->GetQuantity();
        s = it->GetSlot();

        file_obj<<str<<" "<<fw<<" "<<fs<<" "<<p<<" "<<q<<" "<<s<<endl;
    }
    file_obj.close();
}

void Project::Read()
{
    ifstream file_obj(("Projects/"+_ProjectName+".txt").c_str(), ios::in );
    if(file_obj.fail())
    {
        cout<<"Error: Opening file!"<<endl;
        return;
    }
    Element obj;
    string str, line;
    int fw,fs,p,q,s;
    while(getline(file_obj, line))
    {
        stringstream ss(line);
        ss>>str>>fw>>fs>>p>>q>>s;
        obj.SetElement(str,fw,fs,p);
        obj.SetQuantity(q);
        obj.SetSlot(s);
        _ProjectElements.push_back(obj);
    }
    file_obj.close();
}

void Project::WriteNOPInFile()
{
    ofstream file_obj("Projects/ProjectsByName.txt", ios::app);
    if(file_obj.fail())
    {
        cout<<"Error: Opening file!"<<endl;
        return;
    }
    file_obj<<_ProjectName<<endl;
    file_obj.close();
}

void Project::TakeProjectNamesFromFile(vector<string> &vec)
{
    ifstream file_obj("Projects/ProjectsByName.txt", ios::in);
    if(file_obj.fail())
    {
        cout<<"Error: Opening file!"<<endl;
        return;
    }
    string line;
    while(getline(file_obj, line))
        vec.push_back(line);
    //file_obj.clear();
    //file_obj.seekg(0, ios::beg);
    file_obj.close();
}

void Project::WriteProjectNamesInFile(vector<string> &vec)
{
    ofstream file_obj("Projects/ProjectsByName.txt", ios::out);
    if(file_obj.fail())
    {
        cout<<"Error: Opening file!"<<endl;
        return;
    }
    for(vector<string>::iterator it = vec.begin() ; it != vec.end() ; it++)
        file_obj<<(*it)<<endl;
    file_obj.close();

}

/***************************************************************ISPISI**************************************************************************************/
void Project::PrintAllProjectNames()
{
    ifstream file_obj("Projects/ProjectsByName.txt", ios::in);
    if(file_obj.fail())
    {
        cout<<"Error: Opening file!"<<endl;
        return;
    }
    string line;
    cout<<"Existing projects:"<<endl;
    while(getline(file_obj,line))
    {
        cout<<line<<endl;
    }
    file_obj.close();
}

/***************************************************************TESTERI*************************************************************************************/

bool Project::TestProjectName()
{
    ifstream file_obj("Projects/ProjectsByName.txt", ios::in);
    if(file_obj.fail())
    {
        cout<<"Error: Opening file!"<<endl;
        return false;
    }
    string str, line;
    while(getline(file_obj, line))
    {
        if(line == _ProjectName)
            return true;
    }
    return false;
}
