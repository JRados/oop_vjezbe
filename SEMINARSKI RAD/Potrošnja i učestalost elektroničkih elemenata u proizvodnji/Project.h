#ifndef PROJECT_H
#define PROJECT_H

#include<string>
#include<vector>
#include "Element.h"
using namespace std;

class Project
{
    private:
        string _ProjectName;
        vector<Element> _ProjectElements;
        int _pcs;
    public:
        //KONSTRUKTOR I DESTRUKTOR
        Project(string str);
        ~Project();

        //GETERI
        string GetProjectName() const;
        vector<Element> GetProjectElements() const;
        int GetPCS() const;

        //SETERI
        void SetProjectName(string str);
        void SetPCS(int pcs);

        //KREIRAJ,OTVORI I IZBRISI PROJEKT
        void CreateProject(vector<Element> db);
        void OpenProject();
        static void DeleteProject(string name);

        //UPIS/ISPIS IZ FILEA
        void Write();
        void Read();
        void WriteNOPInFile();
        static void TakeProjectNamesFromFile(vector<string> &vec);
        static void WriteProjectNamesInFile(vector<string> &vec);

        //ISPISI
        static void PrintAllProjectNames();

        //TESTERI
        bool TestProjectName();

};
#endif // PROJECT_H
