#include"Menu.h"

#include<iostream>
using namespace std;

void PressEnter()
{
    cin.ignore();
    cout<<endl<<endl<<"Press ENTER to continue..."<<endl<<endl;
    cin.get();
}

void PressEnter02()
{
    cout<<endl<<endl<<"Press ENTER to continue..."<<endl<<endl;
    cin.get();
}

int MainMenu()
{
    int MenuNumber;
    for(int i=0; i<25; i++){cout<<"\n";}
    cout<<"\n";
    cout<<"                                         ************************************                                          "<<endl;
    cout<<"                                         *               MENU               *                                          "<<endl;
    cout<<"                                         ************************************                                          "<<endl<<endl;
    cout<<"************************************************************************************************************************"<<endl;
    cout<<"Choose with what data you want to manipulate:"<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    cout<<"                                        |     1.ELEMENTS                     |                                         "<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    cout<<"                                        |     2.PROJECTS                     |                                         "<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    cout<<"                                        |     3.MACHINE                      |                                         "<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    cout<<"                                        |     4.USAGE OF ELEMENTS            |                                         "<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    cout<<"                                        |     0.END PROGRAM                  |                                         "<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    for(int i = 0; i<8; i++){cout<<"\n";}
    cout<<endl<<"Enter number of choice:\t";

    while(!(cin>>MenuNumber) || ((MenuNumber < 0) || (MenuNumber > 4)))
        {
             cout<<"ERROR: Numbers 0 or 4 must be entered!"<<endl;
             cin.clear();
             cin.ignore(132,'\n');
        }
        return MenuNumber;
}

int ElementsMenu()
{
    int MenuNumber;
    for(int i=0; i<25; i++){cout<<"\n";}
    cout<<"                                         ************************************                                          "<<endl;
    cout<<"                                         *             ELEMENTS             *                                          "<<endl;
    cout<<"                                         ************************************                                          "<<endl<<endl;
    cout<<"************************************************************************************************************************"<<endl;
    cout<<"Choose with what data you want to manipulate:"<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    cout<<"                                        |     1.PRINT ALL ELEMENTS           |                                         "<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    cout<<"                                        |     2.CREATE NEW ELEMENT           |                                         "<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    cout<<"                                        |     3.DELETE ELEMENT               |                                         "<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    cout<<"                                        |     4.UPDATE ELEMENT               |                                         "<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    cout<<"                                        |     5.GET BACK TO MAIN MENU        |                                         "<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    for(int i = 0; i<8; i++){cout<<"\n";}
    cout<<endl<<"Enter number of choice:\t";
    while(!(cin>>MenuNumber) || ((MenuNumber < 1) || (MenuNumber > 5)))
        {
             cout<<"ERROR: Numbers between 1 and 5 must be entered!"<<endl;
             cin.clear();
             cin.ignore(132,'\n');
        }
    return MenuNumber;
}

int ProjectsMenu()
{
    int MenuNumber;
    for(int i=0; i<25; i++){cout<<"\n";}
    cout<<"                                         ************************************                                          "<<endl;
    cout<<"                                         *             PROJECTS             *                                          "<<endl;
    cout<<"                                         ************************************                                          "<<endl<<endl;
    cout<<"************************************************************************************************************************"<<endl;
    cout<<"Choose with what data you want to manipulate:"<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    cout<<"                                        |     1.PRINT ALL PROJECTS           |                                         "<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    cout<<"                                        |     2.CREATE PROJECT               |                                         "<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    cout<<"                                        |     3.DELETE PROJECT               |                                         "<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    cout<<"                                        |     4.GET BACK TO MAIN MENU        |                                         "<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    for(int i = 0; i<10; i++){cout<<"\n";}
    cout<<endl<<"Enter number of choice:\t";
    while(!(cin>>MenuNumber) || ((MenuNumber < 1) || (MenuNumber > 4)))
        {
             cout<<"ERROR: Numbers between 1 and 4 must be entered!"<<endl;
             cin.clear();
             cin.ignore(132,'\n');
        }
    return MenuNumber;
}

int MachineMenu()
{
    int MenuNumber;
    for(int i=0; i<25; i++){cout<<"\n";}
    cout<<"                                         ************************************                                          "<<endl;
    cout<<"                                         *             MACHINE              *                                          "<<endl;
    cout<<"                                         ************************************                                          "<<endl<<endl;
    cout<<"************************************************************************************************************************"<<endl;
    cout<<"Choose with what data you want to manipulate:"<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    cout<<"                                        |     1.PRINT CURRENT STATE          |                                         "<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    cout<<"                                        |     2.CHANGE ELEMENT               |                                         "<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    cout<<"                                        |     3.ACTIVATE PROJECT             |                                         "<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    cout<<"                                        |     4.GET BACK TO MAIN MENU        |                                         "<<endl;
    cout<<"                                         ------------------------------------                                          "<<endl;
    for(int i = 0; i<10; i++){cout<<"\n";}
    cout<<endl<<"Enter number of choice:\t";
    while(!(cin>>MenuNumber) || ((MenuNumber < 1) || (MenuNumber > 4)))
        {
             cout<<"ERROR: Numbers between 1 and 4 must be entered!"<<endl;
             cin.clear();
             cin.ignore(132,'\n');
        }
    return MenuNumber;
}
