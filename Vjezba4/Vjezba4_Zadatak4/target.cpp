#include <iostream>
#include"target.h"

int target::isHit(double gun)
{
    if( gun < point_begining.getHeight() || gun > point_end.getHeight() )
        return 0;
    else
        return 1;
}

const void target::generate_target(int low, int high)
{
    point_begining.set_random(low,high);
    point_end.set_variables(low,high);
}

double target::get_low_height()
{
    return point_begining.getHeight();
}

double target::get_high_height()
{
    return point_end.getHeight();
}
