#ifndef WEAPON_H


#define WEAPON_H

#include "Point.h"

class weapon{
    private:
Point gun_point;
        const int clip = 6;
        int bullets = 6;

    public:

        const void reload();
        const void shoot(int &counter);
        const void clip_state();
        const void set_gun_point(int low, int high);
        double get_gun_height();

};
#endif // WEAPON_H
