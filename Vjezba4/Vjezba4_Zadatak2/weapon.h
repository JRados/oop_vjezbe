#ifndef WEAPON_H


#define WEAPON_H

#include "Point.h"

class weapon{
    private:
        Point T;
        const int clip = 6;
        int bullets = 0;

    public:
        void reload();
        void shoot();
        void clip_state();

};
#endif // WEAPON_H
