#include "Point.h"

#include<iostream>

#include<math.h>
#include<time.h>

void Point::set_variables(double x, double y, double z)
{
    width = x;
    height = y;
    lenght = z;
}

void Point::set_random()
{
    height = rand();
    width = rand();
    lenght = rand();
}

double Point::getWidth()
{
    return width;
}

double Point::getHeight()
{
    return height;
}

double Point::getLenght()
{
    return lenght;
}

double Point::lenght2D(double wid, double hei)
{
   return sqrt(pow((width - wid), 2) + pow((height - hei),2));
}

double Point::lenght3D(double wid, double hei, double len)
{
   return sqrt(pow((wid - width), 2) + pow((hei - height),2) + pow((len - lenght),2));
}
