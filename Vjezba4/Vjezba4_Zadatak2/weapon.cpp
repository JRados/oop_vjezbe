#include "iostream"
#include "Point.h"
#include "weapon.h"

void weapon::reload()
{
    bullets = clip;
}

void weapon::shoot()
{
    if(bullets == 0)
    {
        std::cout<<"Reloading!"<<std::endl;
        reload();
    }
    bullets--;
}

void weapon::clip_state()
{
    std::cout<<bullets<<std::endl;
}
