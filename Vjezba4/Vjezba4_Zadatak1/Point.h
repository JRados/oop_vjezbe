#ifndef POINT_H
#define POINT_H


class Point
{
    private:

        double width;
        double height;
        double lenght;


    public:

        void set_variables(double x=0, double y=0, double z=0);

        void set_random();

        double getWidth();
        double getHeight();
        double getLenght();

        double lenght2D(double wid, double hei);
        double lenght3D(double wid, double hei, double len);

};
#endif // POINT_H
