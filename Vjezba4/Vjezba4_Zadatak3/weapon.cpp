#include "iostream"
#include "Point.h"
#include "weapon.h"

const void weapon::reload()
{
    bullets = clip;
}

const void weapon::shoot(int &counter)
{
    if(bullets == 0)
    {
        std::cout<<"You hit "<<counter<<" targets with the clip."<<std::endl;
        counter = 0;
        std::cout<<"Reloading!"<<std::endl;
        reload();
    }
    bullets--;
}

const void weapon::clip_state()
{
    std::cout<<bullets<<std::endl;
}

const void weapon::set_gun_point(int low, int high)
{
    gun_point.set_random(low,high);
}

double weapon::get_gun_height()
{
    return gun_point.getHeight();
}
