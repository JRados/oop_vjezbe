#ifndef TARGET_H
#define TARGET_H

#include "Point.h"

class target{

    private:
        Point point_begining;
        Point point_end;
        const double target_height = 5;
        int hit;

    public:
        int isHit(double gun);
        const void generate_target(int low, int high);
        double get_low_height();
        double get_high_height();
};

#endif // TARGET_H
